<div class="20_80" id="main-content" role="main">
	<div class="portlet-layout row-fluid">
		<div class="portlet-column portlet-column-only">
			<div class="portlet-layout row-fluid">
				<div class="portlet-column portlet-column-first span2 col_right" id="column-1">
					$processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
				</div>

				<div class="portlet-column portlet-column-last span10 col-left" id="column-2">
					$processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")
				</div>
			</div>
		</div>
	</div>
</div>
