<%@page import="vn.dtt.sol.cmon.dm.business.DataItemBusiness"%>
<%@page import="vn.dtt.sol.cmon.dm.dao.model.DATAITEM"%>
<%@page import="vn.dtt.sol.cmon.dm.business.DataGroupBusiness"%>
<%@page import="vn.dtt.sol.cmon.dm.dao.model.DATAGROUP"%>
<%
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>

<%@ include file="/init.jsp" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%
	List<DATAGROUP> lsDataGroup = DataGroupBusiness.getAllDataGroup();
%>
<ul>
<%
	for (DATAGROUP dataGroup : lsDataGroup) {
%>
	<li>
		<%= dataGroup.getName() %>
		<ul>
			<%
				List<DATAITEM> lsDataLevel1 = DataItemBusiness.getInLevel1(dataGroup.getId());
				for (DATAITEM dataItem1 : lsDataLevel1) {
			%>
					<li>
						<%= dataItem1.getName() %>
						<ul>
							<%
								List<DATAITEM> lsDataLevel2 = DataItemBusiness.getInLevel2(dataGroup.getId(), dataItem1.getNode1());
								for (DATAITEM dataItem2 : lsDataLevel2) {

							%>
								<li><%= dataItem2.getName() %></li>
							<%
								}
							%>
						</ul>
					</li>
			<% }%>
		</ul>
	</li>
<%
	}
%>
</ul>