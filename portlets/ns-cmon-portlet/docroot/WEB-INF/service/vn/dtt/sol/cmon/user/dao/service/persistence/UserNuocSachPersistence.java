/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.sol.cmon.user.dao.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import vn.dtt.sol.cmon.user.dao.model.UserNuocSach;

/**
 * The persistence interface for the user nuoc sach service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author khoa.vu
 * @see UserNuocSachPersistenceImpl
 * @see UserNuocSachUtil
 * @generated
 */
public interface UserNuocSachPersistence extends BasePersistence<UserNuocSach> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserNuocSachUtil} to access the user nuoc sach persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the user nuoc sach where mapUserId = &#63; or throws a {@link vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException} if it could not be found.
	*
	* @param mapUserId the map user ID
	* @return the matching user nuoc sach
	* @throws vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException if a matching user nuoc sach could not be found
	* @throws SystemException if a system exception occurred
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach findByM_U_I(
		long mapUserId)
		throws com.liferay.portal.kernel.exception.SystemException,
			vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException;

	/**
	* Returns the user nuoc sach where mapUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param mapUserId the map user ID
	* @return the matching user nuoc sach, or <code>null</code> if a matching user nuoc sach could not be found
	* @throws SystemException if a system exception occurred
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach fetchByM_U_I(
		long mapUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user nuoc sach where mapUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param mapUserId the map user ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching user nuoc sach, or <code>null</code> if a matching user nuoc sach could not be found
	* @throws SystemException if a system exception occurred
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach fetchByM_U_I(
		long mapUserId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the user nuoc sach where mapUserId = &#63; from the database.
	*
	* @param mapUserId the map user ID
	* @return the user nuoc sach that was removed
	* @throws SystemException if a system exception occurred
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach removeByM_U_I(
		long mapUserId)
		throws com.liferay.portal.kernel.exception.SystemException,
			vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException;

	/**
	* Returns the number of user nuoc sachs where mapUserId = &#63;.
	*
	* @param mapUserId the map user ID
	* @return the number of matching user nuoc sachs
	* @throws SystemException if a system exception occurred
	*/
	public int countByM_U_I(long mapUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the user nuoc sach in the entity cache if it is enabled.
	*
	* @param userNuocSach the user nuoc sach
	*/
	public void cacheResult(
		vn.dtt.sol.cmon.user.dao.model.UserNuocSach userNuocSach);

	/**
	* Caches the user nuoc sachs in the entity cache if it is enabled.
	*
	* @param userNuocSachs the user nuoc sachs
	*/
	public void cacheResult(
		java.util.List<vn.dtt.sol.cmon.user.dao.model.UserNuocSach> userNuocSachs);

	/**
	* Creates a new user nuoc sach with the primary key. Does not add the user nuoc sach to the database.
	*
	* @param userNuocSachPK the primary key for the new user nuoc sach
	* @return the new user nuoc sach
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach create(
		UserNuocSachPK userNuocSachPK);

	/**
	* Removes the user nuoc sach with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userNuocSachPK the primary key of the user nuoc sach
	* @return the user nuoc sach that was removed
	* @throws vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException if a user nuoc sach with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach remove(
		UserNuocSachPK userNuocSachPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException;

	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach updateImpl(
		vn.dtt.sol.cmon.user.dao.model.UserNuocSach userNuocSach)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user nuoc sach with the primary key or throws a {@link vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException} if it could not be found.
	*
	* @param userNuocSachPK the primary key of the user nuoc sach
	* @return the user nuoc sach
	* @throws vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException if a user nuoc sach with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach findByPrimaryKey(
		UserNuocSachPK userNuocSachPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			vn.dtt.sol.cmon.user.dao.NoSuchUserNuocSachException;

	/**
	* Returns the user nuoc sach with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userNuocSachPK the primary key of the user nuoc sach
	* @return the user nuoc sach, or <code>null</code> if a user nuoc sach with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach fetchByPrimaryKey(
		UserNuocSachPK userNuocSachPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user nuoc sachs.
	*
	* @return the user nuoc sachs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<vn.dtt.sol.cmon.user.dao.model.UserNuocSach> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user nuoc sachs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.dtt.sol.cmon.user.dao.model.impl.UserNuocSachModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user nuoc sachs
	* @param end the upper bound of the range of user nuoc sachs (not inclusive)
	* @return the range of user nuoc sachs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<vn.dtt.sol.cmon.user.dao.model.UserNuocSach> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user nuoc sachs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.dtt.sol.cmon.user.dao.model.impl.UserNuocSachModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user nuoc sachs
	* @param end the upper bound of the range of user nuoc sachs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user nuoc sachs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<vn.dtt.sol.cmon.user.dao.model.UserNuocSach> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user nuoc sachs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user nuoc sachs.
	*
	* @return the number of user nuoc sachs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}