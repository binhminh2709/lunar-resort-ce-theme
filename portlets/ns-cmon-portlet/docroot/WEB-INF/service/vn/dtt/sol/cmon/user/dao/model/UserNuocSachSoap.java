/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.sol.cmon.user.dao.model;

import vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author khoa.vu
 * @generated
 */
public class UserNuocSachSoap implements Serializable {
	public static UserNuocSachSoap toSoapModel(UserNuocSach model) {
		UserNuocSachSoap soapModel = new UserNuocSachSoap();

		soapModel.setId(model.getId());
		soapModel.setMapUserId(model.getMapUserId());
		soapModel.setTen(model.getTen());
		soapModel.setTrangThai(model.getTrangThai());
		soapModel.setNgayTao(model.getNgayTao());
		soapModel.setNgaySua(model.getNgaySua());
		soapModel.setNguoiTao(model.getNguoiTao());
		soapModel.setNguoiSua(model.getNguoiSua());
		soapModel.setToChucId(model.getToChucId());
		soapModel.setScreenName(model.getScreenName());
		soapModel.setEmailAddress(model.getEmailAddress());
		soapModel.setNgaySinh(model.getNgaySinh());
		soapModel.setGioiTinh(model.getGioiTinh());
		soapModel.setMatKhau(model.getMatKhau());

		return soapModel;
	}

	public static UserNuocSachSoap[] toSoapModels(UserNuocSach[] models) {
		UserNuocSachSoap[] soapModels = new UserNuocSachSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserNuocSachSoap[][] toSoapModels(UserNuocSach[][] models) {
		UserNuocSachSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserNuocSachSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserNuocSachSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserNuocSachSoap[] toSoapModels(List<UserNuocSach> models) {
		List<UserNuocSachSoap> soapModels = new ArrayList<UserNuocSachSoap>(models.size());

		for (UserNuocSach model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserNuocSachSoap[soapModels.size()]);
	}

	public UserNuocSachSoap() {
	}

	public UserNuocSachPK getPrimaryKey() {
		return new UserNuocSachPK(_id, _mapUserId);
	}

	public void setPrimaryKey(UserNuocSachPK pk) {
		setId(pk.id);
		setMapUserId(pk.mapUserId);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public long getMapUserId() {
		return _mapUserId;
	}

	public void setMapUserId(long mapUserId) {
		_mapUserId = mapUserId;
	}

	public String getTen() {
		return _ten;
	}

	public void setTen(String ten) {
		_ten = ten;
	}

	public boolean getTrangThai() {
		return _trangThai;
	}

	public boolean isTrangThai() {
		return _trangThai;
	}

	public void setTrangThai(boolean trangThai) {
		_trangThai = trangThai;
	}

	public Date getNgayTao() {
		return _ngayTao;
	}

	public void setNgayTao(Date ngayTao) {
		_ngayTao = ngayTao;
	}

	public Date getNgaySua() {
		return _ngaySua;
	}

	public void setNgaySua(Date ngaySua) {
		_ngaySua = ngaySua;
	}

	public String getNguoiTao() {
		return _nguoiTao;
	}

	public void setNguoiTao(String nguoiTao) {
		_nguoiTao = nguoiTao;
	}

	public String getNguoiSua() {
		return _nguoiSua;
	}

	public void setNguoiSua(String nguoiSua) {
		_nguoiSua = nguoiSua;
	}

	public long getToChucId() {
		return _toChucId;
	}

	public void setToChucId(long toChucId) {
		_toChucId = toChucId;
	}

	public String getScreenName() {
		return _screenName;
	}

	public void setScreenName(String screenName) {
		_screenName = screenName;
	}

	public String getEmailAddress() {
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	public Date getNgaySinh() {
		return _ngaySinh;
	}

	public void setNgaySinh(Date ngaySinh) {
		_ngaySinh = ngaySinh;
	}

	public boolean getGioiTinh() {
		return _gioiTinh;
	}

	public boolean isGioiTinh() {
		return _gioiTinh;
	}

	public void setGioiTinh(boolean gioiTinh) {
		_gioiTinh = gioiTinh;
	}

	public String getMatKhau() {
		return _matKhau;
	}

	public void setMatKhau(String matKhau) {
		_matKhau = matKhau;
	}

	private long _id;
	private long _mapUserId;
	private String _ten;
	private boolean _trangThai;
	private Date _ngayTao;
	private Date _ngaySua;
	private String _nguoiTao;
	private String _nguoiSua;
	private long _toChucId;
	private String _screenName;
	private String _emailAddress;
	private Date _ngaySinh;
	private boolean _gioiTinh;
	private String _matKhau;
}