/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.sol.cmon.user.dao.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author khoa.vu
 */
public class UserNuocSachPK implements Comparable<UserNuocSachPK>, Serializable {
	public long id;
	public long mapUserId;

	public UserNuocSachPK() {
	}

	public UserNuocSachPK(long id, long mapUserId) {
		this.id = id;
		this.mapUserId = mapUserId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMapUserId() {
		return mapUserId;
	}

	public void setMapUserId(long mapUserId) {
		this.mapUserId = mapUserId;
	}

	@Override
	public int compareTo(UserNuocSachPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (id < pk.id) {
			value = -1;
		}
		else if (id > pk.id) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (mapUserId < pk.mapUserId) {
			value = -1;
		}
		else if (mapUserId > pk.mapUserId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserNuocSachPK)) {
			return false;
		}

		UserNuocSachPK pk = (UserNuocSachPK)obj;

		if ((id == pk.id) && (mapUserId == pk.mapUserId)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(id) + String.valueOf(mapUserId)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("id");
		sb.append(StringPool.EQUAL);
		sb.append(id);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("mapUserId");
		sb.append(StringPool.EQUAL);
		sb.append(mapUserId);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}