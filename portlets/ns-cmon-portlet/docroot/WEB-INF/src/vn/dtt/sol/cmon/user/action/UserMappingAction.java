package vn.dtt.sol.cmon.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;

import vn.dtt.sol.cmon.user.dao.service.UserMappingLocalServiceUtil;
import vn.dtt.sol.cmon.util.MessageErrors;

public class UserMappingAction {
	public static final String ID = "id";
	public static final String USER_ID = "userId";
	public static final String USER_TYPE = "typeUser";
	public static final String ORG_CODE1 = "orgCode1";
	public static final String ORG_CODE2 = "orgCode2";

	public void addUserMapping(ActionRequest request, ActionResponse response)
			throws Exception {

		long id = ParamUtil.getLong(request, ID);
		long userId = ParamUtil.getLong(request, USER_ID);
		int typeUser = ParamUtil.getInteger(request, USER_TYPE);
		String orgCode1 = ParamUtil.getString(request, ORG_CODE1);
		String orgCode2 = ParamUtil.getString(request, ORG_CODE1);

		String cmd = ParamUtil.getString(request, Constants.CMD);

		List<String> errors = new ArrayList<String>();

		boolean valid = validationInput(userId, typeUser, errors);

		if (valid) {
			if (cmd.contentEquals(Constants.ADD)
					&& cmd.contentEquals(Constants.UPDATE)) {

				UserMappingLocalServiceUtil.addUserMapping(id, userId,
						typeUser, orgCode1, orgCode2);

				SessionMessages.add(request, "usermapping-saved-successfully");
			}
		}
		
		if (errors.size() != 0) {
			for (String error : errors) {
				SessionMessages.add(request, error);
			}
		}
	}

	/**
	 * @param userId
	 * @param userType
	 * @param errors
	 * @return
	 */
	private boolean validationInput(long userId, int userType,
			List<String> errors) {

		boolean result = true;

		if (userId == 0) {
			errors.add(MessageErrors.EMPTY_USER_LIFERAY);
			result = false;
		}

		if (userType == 0) {
			errors.add(MessageErrors.EMPTY_USER_TYPE);
			result = false;
		}

		return result;
	}

}
