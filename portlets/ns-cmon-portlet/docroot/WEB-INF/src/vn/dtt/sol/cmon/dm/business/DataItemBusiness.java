package vn.dtt.sol.cmon.dm.business;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import vn.dtt.sol.cmon.dm.dao.model.DATAITEM;
import vn.dtt.sol.cmon.dm.dao.service.DATAITEMLocalServiceUtil;

public class DataItemBusiness {
	/**
	 * Get DATAITEM by dataGroupId
	 * 
	 * @param dataGroupId
	 * @return
	 */
	public static List<DATAITEM> getByDataGroupId(long dataGroupId) {
		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupId(dataGroupId);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;
	}

	/**
	 * Get DATAITEM by dataGroupId in range
	 * 
	 * @param dataGroupId
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<DATAITEM> getByDataGroupId(long dataGroupId, int start,
			int end) {
		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupId(dataGroupId, start,
					end);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;
	}

	/**
	 * Count DATAITEM by dataGroupId
	 * 
	 * @param dataGroupId
	 * @return
	 */
	public static int countByDataGroupId(long dataGroupId) {
		int count = 0;

		try {
			count = DATAITEMLocalServiceUtil.countByDataGroupId(dataGroupId);
		} catch (Exception e) {
			_log.error(e);
		}

		return count;
	}

	/**
	 * Get DATAITEM in Level1
	 * 
	 * @param dataGroupId
	 * @return
	 */
	public static List<DATAITEM> getInLevel1(long dataGroupId) {

		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupLevel1(dataGroupId);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;

	}

	/**
	 * Get DATAITEM in Level1 in range
	 * 
	 * @param dataGroupId
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<DATAITEM> getInLevel1(long dataGroupId, int start,
			int end) {

		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupLevel1(dataGroupId,
					start, end);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;

	}

	/**
	 * Count DATAITEM in Level1
	 * 
	 * @param dataGroupId
	 * @return
	 */
	public static int countInLevel1(long dataGroupId) {
		int count = 0;

		try {
			count = DATAITEMLocalServiceUtil
					.countByDataGroupLevel1(dataGroupId);
		} catch (Exception e) {
			_log.error(e);
		}

		return count;
	}

	/**
	 * Get DATAITEM in Level2
	 * 
	 * @param dataGroupId
	 * @param node1
	 * @return
	 */
	public static List<DATAITEM> getInLevel2(long dataGroupId, String node1) {

		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupLevel2(dataGroupId,
					node1);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;

	}

	/**
	 * Get DATAITEM in Level2 in range
	 * 
	 * @param dataGroupId
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<DATAITEM> getInLevel2(long dataGroupId, String node1,
			int start, int end) {

		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupLevel2(dataGroupId,
					node1, start, end);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;

	}

	/**
	 * Count DATAITEM in Level2
	 * 
	 * @param dataGroupId
	 * @return
	 */
	public static int countInLevel2(long dataGroupId, String node1) {
		int count = 0;

		try {
			count = DATAITEMLocalServiceUtil.countByDataGroupLevel2(
					dataGroupId, node1);
		} catch (Exception e) {
			_log.error(e);
		}

		return count;
	}

	/**
	 * Get DATAITEM in Level3
	 * 
	 * @param dataGroupId
	 * @param node1
	 * @param node2
	 * @return
	 */
	public static List<DATAITEM> getInLevel3(long dataGroupId, String node1,
			String node2) {

		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupLevel3(dataGroupId,
					node1, node2);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;

	}

	/**
	 * Get DATAITEM in Level3
	 * 
	 * @param dataGroupId
	 * @param node1
	 * @param node2
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<DATAITEM> getInLevel3(long dataGroupId, String node1,
			String node2, int start, int end) {

		List<DATAITEM> ls = new ArrayList<DATAITEM>();

		try {
			ls = DATAITEMLocalServiceUtil.getByDataGroupLevel3(dataGroupId,
					node1, node2, start, end);
		} catch (Exception e) {
			_log.error(e);
		}

		return ls;

	}

	/**
	 * Count DATAITEM in Level3s
	 * 
	 * @param dataGroupId
	 * @param node1
	 * @param node2
	 * @return
	 */
	public static int countInLevel3(long dataGroupId, String node1, String node2) {
		int count = 0;

		try {
			count = DATAITEMLocalServiceUtil.countByDataGroupLevel3(
					dataGroupId, node1, node2);
		} catch (Exception e) {
			_log.error(e);
		}

		return count;
	}

	private static Log _log = LogFactoryUtil.getLog(DataItemBusiness.class);
}
