package com.dtt.nuocsach.bean;

public class CheckKiemDemKeHoach {
	private int id;
	private int nuoc;
	private int vesinh;
	private String kiemdemvien;
	private String giamsatvien;
	private String tinh;
	private String code;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNuoc() {
		return nuoc;
	}

	public void setNuoc(int nuoc) {
		this.nuoc = nuoc;
	}

	public int getVesinh() {
		return vesinh;
	}

	public void setVesinh(int vesinh) {
		this.vesinh = vesinh;
	}

	public String getKiemdemvien() {
		return kiemdemvien;
	}

	public void setKiemdemvien(String kiemdemvien) {
		this.kiemdemvien = kiemdemvien;
	}

	public String getGiamsatvien() {
		return giamsatvien;
	}

	public void setGiamsatvien(String giamsatvien) {
		this.giamsatvien = giamsatvien;
	}

	public String getTinh() {
		return tinh;
	}

	public void setTinh(String tinh) {
		this.tinh = tinh;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
