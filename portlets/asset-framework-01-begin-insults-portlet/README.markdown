# Deployment Instructions

1. Copy this portlet into a Liferay plugins SDK.
2. Rename the -portlet project directory to `insults-portlet`. **Important:** 
You must rename the portlet as specified in order for it to work properly.
3. Deploy the portlet from its project directory using command `ant deploy`.