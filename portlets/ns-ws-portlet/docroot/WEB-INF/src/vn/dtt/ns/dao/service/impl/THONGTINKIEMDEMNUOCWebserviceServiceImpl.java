/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.ns.dao.service.impl;

import vn.dtt.ns.dao.service.base.THONGTINKIEMDEMNUOCWebserviceServiceBaseImpl;

/**
 * The implementation of the t h o n g t i n k i e m d e m n u o c webservice remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link vn.dtt.ns.dao.service.THONGTINKIEMDEMNUOCWebserviceService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author binhta
 * @see vn.dtt.ns.dao.service.base.THONGTINKIEMDEMNUOCWebserviceServiceBaseImpl
 * @see vn.dtt.ns.dao.service.THONGTINKIEMDEMNUOCWebserviceServiceUtil
 */
public class THONGTINKIEMDEMNUOCWebserviceServiceImpl
	extends THONGTINKIEMDEMNUOCWebserviceServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link vn.dtt.ns.dao.service.THONGTINKIEMDEMNUOCWebserviceServiceUtil} to access the t h o n g t i n k i e m d e m n u o c webservice remote service.
	 */
}