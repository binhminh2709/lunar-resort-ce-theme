package vn.dtt.ns.bean;

public class CheckIDUpdateCC {
	private int id_vesinhct;
	private String id_mobile;
	private int id_kehoach;
	private int id_thongtinvsct;

	public int getId_vesinhct() {
		return id_vesinhct;
	}

	public void setId_vesinhct(int id_vesinhct) {
		this.id_vesinhct = id_vesinhct;
	}

	public String getId_mobile() {
		return id_mobile;
	}

	public void setId_mobile(String id_mobile) {
		this.id_mobile = id_mobile;
	}

	public int getId_kehoach() {
		return id_kehoach;
	}

	public void setId_kehoach(int id_kehoach) {
		this.id_kehoach = id_kehoach;
	}

	public int getId_thongtinvsct() {
		return id_thongtinvsct;
	}

	public void setId_thongtinvsct(int id_thongtinvsct) {
		this.id_thongtinvsct = id_thongtinvsct;
	}

}
