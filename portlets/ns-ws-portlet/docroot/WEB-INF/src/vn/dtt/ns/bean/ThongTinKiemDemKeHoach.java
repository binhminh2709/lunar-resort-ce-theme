package vn.dtt.ns.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

public class ThongTinKiemDemKeHoach {
	private Integer id;
	private Integer nam;
	private Integer kehoachkiemdemid;
	private Date tungay;
	private Date denngay;
	private Integer daketthucdieutra;
	private Date ngaydong;

	public ThongTinKiemDemKeHoach(Integer id, Integer kehoachkiemdemid, Integer daketthucdieutra, Integer nam,
			Date tungay, Date denngay, Date ngaydong) {
		super();
		this.id = id;
		this.kehoachkiemdemid = kehoachkiemdemid;
		this.tungay = tungay;
		this.denngay = denngay;
		this.daketthucdieutra = daketthucdieutra;
		this.nam = nam;
		this.ngaydong = ngaydong;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getKehoachkiemdemid() {
		return kehoachkiemdemid;
	}

	public void setKehoachkiemdemid(Integer kehoachkiemdemid) {
		this.kehoachkiemdemid = kehoachkiemdemid;
	}

	public Date getTungay() {
		return tungay;
	}

	public void setTungay(Date tungay) {
		this.tungay = tungay;
	}

	public Date getDenngay() {
		return denngay;
	}

	public void setDenngay(Date denngay) {
		this.denngay = denngay;
	}

	public Integer getDaketthucdieutra() {
		return daketthucdieutra;
	}

	public void setDaketthucdieutra(Integer daketthucdieutra) {
		this.daketthucdieutra = daketthucdieutra;
	}

	public Integer getNam() {
		return nam;
	}

	public void setNam(Integer nam) {
		this.nam = nam;
	}
	public Date getNgaydong() {
		return ngaydong;
	}

	public void setNgaydong(Date ngaydong) {
		this.ngaydong = ngaydong;
	}

}
