package vn.dtt.ns.bean;

public class CheckIdUpdate {
	private int id_daunoi;
	private String id_mobile;
	private int id_kehoach;
	private int id_thongtinkiemdem;
	private int isdelete;

	public int getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(int isdelete) {
		this.isdelete = isdelete;
	}

	public int getId_daunoi() {
		return id_daunoi;
	}

	public void setId_daunoi(int id_daunoi) {
		this.id_daunoi = id_daunoi;
	}

	public String getId_mobile() {
		return id_mobile;
	}

	public void setId_mobile(String id_mobile) {
		this.id_mobile = id_mobile;
	}

	public int getId_kehoach() {
		return id_kehoach;
	}

	public void setId_kehoach(int id_kehoach) {
		this.id_kehoach = id_kehoach;
	}

	public int getId_thongtinkiemdem() {
		return id_thongtinkiemdem;
	}

	public void setId_thongtinkiemdem(int id_thongtinkiemdem) {
		this.id_thongtinkiemdem = id_thongtinkiemdem;
	}

}
