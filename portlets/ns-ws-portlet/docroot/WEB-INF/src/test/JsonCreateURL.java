package test;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

public class JsonCreateURL {
	 private static final String WS_USER = "test@liferay.com";
	 private static final String WS_PASS = "test";
	 private static final String WS_HOST = "localhost";
	 private static final int WS_POST = 8080;
	 private static final String WS_HTTP = "http";
	public static HttpResponse createURL(String user, String pass, String WsJson, UrlEncodedFormEntity entity) throws ClientProtocolException, IOException{
		 HttpResponse resp = null;
		 HttpHost targetHost = new HttpHost(WS_HOST, WS_POST, WS_HTTP);
		 DefaultHttpClient httpclient = new DefaultHttpClient();
	        httpclient.getCredentialsProvider().setCredentials(
	                new AuthScope(targetHost.getHostName(), targetHost.getPort()),
	                new UsernamePasswordCredentials(WS_USER, WS_PASS));
		// Create AuthCache instance
	     AuthCache authCache = new BasicAuthCache();
	    // auth cache
	     BasicScheme basicAuth = new BasicScheme();
	     authCache.put(targetHost, basicAuth);
		// Add AuthCache to the execution context
	     BasicHttpContext ctx = new BasicHttpContext();
	     ctx.setAttribute(ClientContext.AUTH_CACHE, authCache);

	     HttpPost post = new HttpPost(WsJson);
	     post.setEntity(entity);
	     
	     resp = httpclient.execute(targetHost, post, ctx);
	     httpclient.getConnectionManager().shutdown();
		 return resp;
	}
}