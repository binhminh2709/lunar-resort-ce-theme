<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<portlet:defineObjects />

<portlet:actionURL var="importdataActionURL" windowState="normal"
	name="importData">
</portlet:actionURL>

<h2>Đồng bộ dữ liệu</h2>

<br />
<form action="<%=importdataActionURL%>" name="studentForm" method="POST">
	<input type="submit" name="importData" id="importData"
		value="Đồng bộ dữ liệu" />
</form>
<% if(SessionMessages.contains(renderRequest.getPortletSession(),"start-import")){%>
<liferay-ui:success key="start-import" message="Đang bắt đầu đồng bộ dữ liệu..." />
<%} %>
<% if(SessionMessages.contains(renderRequest.getPortletSession(),"end-import")){%>
<liferay-ui:success key="end-import" message="Đồng bộ dữ liệu thành công..." />
<%} %>