/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException;
import dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc;
import dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocImpl;
import dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the thong tin kiem dem nuoc service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author quangtrung
 * @see ThongTinKiemDemNuocPersistence
 * @see ThongTinKiemDemNuocUtil
 * @generated
 */
public class ThongTinKiemDemNuocPersistenceImpl extends BasePersistenceImpl<ThongTinKiemDemNuoc>
	implements ThongTinKiemDemNuocPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ThongTinKiemDemNuocUtil} to access the thong tin kiem dem nuoc persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ThongTinKiemDemNuocImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocModelImpl.FINDER_CACHE_ENABLED,
			ThongTinKiemDemNuocImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocModelImpl.FINDER_CACHE_ENABLED,
			ThongTinKiemDemNuocImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_D_DAUNOINUOCID =
		new FinderPath(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocModelImpl.FINDER_CACHE_ENABLED,
			ThongTinKiemDemNuocImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByD_dauNoiNuocId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_D_DAUNOINUOCID =
		new FinderPath(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocModelImpl.FINDER_CACHE_ENABLED,
			ThongTinKiemDemNuocImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByD_dauNoiNuocId",
			new String[] { Long.class.getName() },
			ThongTinKiemDemNuocModelImpl.DAUNOINUOCID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_D_DAUNOINUOCID = new FinderPath(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByD_dauNoiNuocId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @return the matching thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThongTinKiemDemNuoc> findByD_dauNoiNuocId(long dauNoiNuocId)
		throws SystemException {
		return findByD_dauNoiNuocId(dauNoiNuocId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @param start the lower bound of the range of thong tin kiem dem nuocs
	 * @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	 * @return the range of matching thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThongTinKiemDemNuoc> findByD_dauNoiNuocId(long dauNoiNuocId,
		int start, int end) throws SystemException {
		return findByD_dauNoiNuocId(dauNoiNuocId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @param start the lower bound of the range of thong tin kiem dem nuocs
	 * @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThongTinKiemDemNuoc> findByD_dauNoiNuocId(long dauNoiNuocId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_D_DAUNOINUOCID;
			finderArgs = new Object[] { dauNoiNuocId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_D_DAUNOINUOCID;
			finderArgs = new Object[] {
					dauNoiNuocId,
					
					start, end, orderByComparator
				};
		}

		List<ThongTinKiemDemNuoc> list = (List<ThongTinKiemDemNuoc>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ThongTinKiemDemNuoc thongTinKiemDemNuoc : list) {
				if ((dauNoiNuocId != thongTinKiemDemNuoc.getDauNoiNuocId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_THONGTINKIEMDEMNUOC_WHERE);

			query.append(_FINDER_COLUMN_D_DAUNOINUOCID_DAUNOINUOCID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ThongTinKiemDemNuocModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dauNoiNuocId);

				if (!pagination) {
					list = (List<ThongTinKiemDemNuoc>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ThongTinKiemDemNuoc>(list);
				}
				else {
					list = (List<ThongTinKiemDemNuoc>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching thong tin kiem dem nuoc
	 * @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a matching thong tin kiem dem nuoc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc findByD_dauNoiNuocId_First(long dauNoiNuocId,
		OrderByComparator orderByComparator)
		throws NoSuchThongTinKiemDemNuocException, SystemException {
		ThongTinKiemDemNuoc thongTinKiemDemNuoc = fetchByD_dauNoiNuocId_First(dauNoiNuocId,
				orderByComparator);

		if (thongTinKiemDemNuoc != null) {
			return thongTinKiemDemNuoc;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dauNoiNuocId=");
		msg.append(dauNoiNuocId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchThongTinKiemDemNuocException(msg.toString());
	}

	/**
	 * Returns the first thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching thong tin kiem dem nuoc, or <code>null</code> if a matching thong tin kiem dem nuoc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc fetchByD_dauNoiNuocId_First(long dauNoiNuocId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ThongTinKiemDemNuoc> list = findByD_dauNoiNuocId(dauNoiNuocId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching thong tin kiem dem nuoc
	 * @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a matching thong tin kiem dem nuoc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc findByD_dauNoiNuocId_Last(long dauNoiNuocId,
		OrderByComparator orderByComparator)
		throws NoSuchThongTinKiemDemNuocException, SystemException {
		ThongTinKiemDemNuoc thongTinKiemDemNuoc = fetchByD_dauNoiNuocId_Last(dauNoiNuocId,
				orderByComparator);

		if (thongTinKiemDemNuoc != null) {
			return thongTinKiemDemNuoc;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dauNoiNuocId=");
		msg.append(dauNoiNuocId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchThongTinKiemDemNuocException(msg.toString());
	}

	/**
	 * Returns the last thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching thong tin kiem dem nuoc, or <code>null</code> if a matching thong tin kiem dem nuoc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc fetchByD_dauNoiNuocId_Last(long dauNoiNuocId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByD_dauNoiNuocId(dauNoiNuocId);

		if (count == 0) {
			return null;
		}

		List<ThongTinKiemDemNuoc> list = findByD_dauNoiNuocId(dauNoiNuocId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the thong tin kiem dem nuocs before and after the current thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	 *
	 * @param id the primary key of the current thong tin kiem dem nuoc
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next thong tin kiem dem nuoc
	 * @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc[] findByD_dauNoiNuocId_PrevAndNext(long id,
		long dauNoiNuocId, OrderByComparator orderByComparator)
		throws NoSuchThongTinKiemDemNuocException, SystemException {
		ThongTinKiemDemNuoc thongTinKiemDemNuoc = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			ThongTinKiemDemNuoc[] array = new ThongTinKiemDemNuocImpl[3];

			array[0] = getByD_dauNoiNuocId_PrevAndNext(session,
					thongTinKiemDemNuoc, dauNoiNuocId, orderByComparator, true);

			array[1] = thongTinKiemDemNuoc;

			array[2] = getByD_dauNoiNuocId_PrevAndNext(session,
					thongTinKiemDemNuoc, dauNoiNuocId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ThongTinKiemDemNuoc getByD_dauNoiNuocId_PrevAndNext(
		Session session, ThongTinKiemDemNuoc thongTinKiemDemNuoc,
		long dauNoiNuocId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_THONGTINKIEMDEMNUOC_WHERE);

		query.append(_FINDER_COLUMN_D_DAUNOINUOCID_DAUNOINUOCID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ThongTinKiemDemNuocModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(dauNoiNuocId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(thongTinKiemDemNuoc);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ThongTinKiemDemNuoc> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the thong tin kiem dem nuocs where dauNoiNuocId = &#63; from the database.
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByD_dauNoiNuocId(long dauNoiNuocId)
		throws SystemException {
		for (ThongTinKiemDemNuoc thongTinKiemDemNuoc : findByD_dauNoiNuocId(
				dauNoiNuocId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(thongTinKiemDemNuoc);
		}
	}

	/**
	 * Returns the number of thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	 *
	 * @param dauNoiNuocId the dau noi nuoc ID
	 * @return the number of matching thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByD_dauNoiNuocId(long dauNoiNuocId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_D_DAUNOINUOCID;

		Object[] finderArgs = new Object[] { dauNoiNuocId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_THONGTINKIEMDEMNUOC_WHERE);

			query.append(_FINDER_COLUMN_D_DAUNOINUOCID_DAUNOINUOCID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dauNoiNuocId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_D_DAUNOINUOCID_DAUNOINUOCID_2 = "thongTinKiemDemNuoc.dauNoiNuocId = ?";

	public ThongTinKiemDemNuocPersistenceImpl() {
		setModelClass(ThongTinKiemDemNuoc.class);
	}

	/**
	 * Caches the thong tin kiem dem nuoc in the entity cache if it is enabled.
	 *
	 * @param thongTinKiemDemNuoc the thong tin kiem dem nuoc
	 */
	@Override
	public void cacheResult(ThongTinKiemDemNuoc thongTinKiemDemNuoc) {
		EntityCacheUtil.putResult(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocImpl.class, thongTinKiemDemNuoc.getPrimaryKey(),
			thongTinKiemDemNuoc);

		thongTinKiemDemNuoc.resetOriginalValues();
	}

	/**
	 * Caches the thong tin kiem dem nuocs in the entity cache if it is enabled.
	 *
	 * @param thongTinKiemDemNuocs the thong tin kiem dem nuocs
	 */
	@Override
	public void cacheResult(List<ThongTinKiemDemNuoc> thongTinKiemDemNuocs) {
		for (ThongTinKiemDemNuoc thongTinKiemDemNuoc : thongTinKiemDemNuocs) {
			if (EntityCacheUtil.getResult(
						ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
						ThongTinKiemDemNuocImpl.class,
						thongTinKiemDemNuoc.getPrimaryKey()) == null) {
				cacheResult(thongTinKiemDemNuoc);
			}
			else {
				thongTinKiemDemNuoc.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all thong tin kiem dem nuocs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ThongTinKiemDemNuocImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ThongTinKiemDemNuocImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the thong tin kiem dem nuoc.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ThongTinKiemDemNuoc thongTinKiemDemNuoc) {
		EntityCacheUtil.removeResult(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocImpl.class, thongTinKiemDemNuoc.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ThongTinKiemDemNuoc> thongTinKiemDemNuocs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ThongTinKiemDemNuoc thongTinKiemDemNuoc : thongTinKiemDemNuocs) {
			EntityCacheUtil.removeResult(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
				ThongTinKiemDemNuocImpl.class,
				thongTinKiemDemNuoc.getPrimaryKey());
		}
	}

	/**
	 * Creates a new thong tin kiem dem nuoc with the primary key. Does not add the thong tin kiem dem nuoc to the database.
	 *
	 * @param id the primary key for the new thong tin kiem dem nuoc
	 * @return the new thong tin kiem dem nuoc
	 */
	@Override
	public ThongTinKiemDemNuoc create(long id) {
		ThongTinKiemDemNuoc thongTinKiemDemNuoc = new ThongTinKiemDemNuocImpl();

		thongTinKiemDemNuoc.setNew(true);
		thongTinKiemDemNuoc.setPrimaryKey(id);

		return thongTinKiemDemNuoc;
	}

	/**
	 * Removes the thong tin kiem dem nuoc with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the thong tin kiem dem nuoc
	 * @return the thong tin kiem dem nuoc that was removed
	 * @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc remove(long id)
		throws NoSuchThongTinKiemDemNuocException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the thong tin kiem dem nuoc with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the thong tin kiem dem nuoc
	 * @return the thong tin kiem dem nuoc that was removed
	 * @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc remove(Serializable primaryKey)
		throws NoSuchThongTinKiemDemNuocException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ThongTinKiemDemNuoc thongTinKiemDemNuoc = (ThongTinKiemDemNuoc)session.get(ThongTinKiemDemNuocImpl.class,
					primaryKey);

			if (thongTinKiemDemNuoc == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchThongTinKiemDemNuocException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(thongTinKiemDemNuoc);
		}
		catch (NoSuchThongTinKiemDemNuocException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ThongTinKiemDemNuoc removeImpl(
		ThongTinKiemDemNuoc thongTinKiemDemNuoc) throws SystemException {
		thongTinKiemDemNuoc = toUnwrappedModel(thongTinKiemDemNuoc);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(thongTinKiemDemNuoc)) {
				thongTinKiemDemNuoc = (ThongTinKiemDemNuoc)session.get(ThongTinKiemDemNuocImpl.class,
						thongTinKiemDemNuoc.getPrimaryKeyObj());
			}

			if (thongTinKiemDemNuoc != null) {
				session.delete(thongTinKiemDemNuoc);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (thongTinKiemDemNuoc != null) {
			clearCache(thongTinKiemDemNuoc);
		}

		return thongTinKiemDemNuoc;
	}

	@Override
	public ThongTinKiemDemNuoc updateImpl(
		dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc thongTinKiemDemNuoc)
		throws SystemException {
		thongTinKiemDemNuoc = toUnwrappedModel(thongTinKiemDemNuoc);

		boolean isNew = thongTinKiemDemNuoc.isNew();

		ThongTinKiemDemNuocModelImpl thongTinKiemDemNuocModelImpl = (ThongTinKiemDemNuocModelImpl)thongTinKiemDemNuoc;

		Session session = null;

		try {
			session = openSession();

			if (thongTinKiemDemNuoc.isNew()) {
				session.save(thongTinKiemDemNuoc);

				thongTinKiemDemNuoc.setNew(false);
			}
			else {
				session.merge(thongTinKiemDemNuoc);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ThongTinKiemDemNuocModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((thongTinKiemDemNuocModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_D_DAUNOINUOCID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						thongTinKiemDemNuocModelImpl.getOriginalDauNoiNuocId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_D_DAUNOINUOCID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_D_DAUNOINUOCID,
					args);

				args = new Object[] {
						thongTinKiemDemNuocModelImpl.getDauNoiNuocId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_D_DAUNOINUOCID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_D_DAUNOINUOCID,
					args);
			}
		}

		EntityCacheUtil.putResult(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
			ThongTinKiemDemNuocImpl.class, thongTinKiemDemNuoc.getPrimaryKey(),
			thongTinKiemDemNuoc);

		return thongTinKiemDemNuoc;
	}

	protected ThongTinKiemDemNuoc toUnwrappedModel(
		ThongTinKiemDemNuoc thongTinKiemDemNuoc) {
		if (thongTinKiemDemNuoc instanceof ThongTinKiemDemNuocImpl) {
			return thongTinKiemDemNuoc;
		}

		ThongTinKiemDemNuocImpl thongTinKiemDemNuocImpl = new ThongTinKiemDemNuocImpl();

		thongTinKiemDemNuocImpl.setNew(thongTinKiemDemNuoc.isNew());
		thongTinKiemDemNuocImpl.setPrimaryKey(thongTinKiemDemNuoc.getPrimaryKey());

		thongTinKiemDemNuocImpl.setId(thongTinKiemDemNuoc.getId());
		thongTinKiemDemNuocImpl.setDauNoiNuocId(thongTinKiemDemNuoc.getDauNoiNuocId());
		thongTinKiemDemNuocImpl.setKeHoachKiemDemNuocId(thongTinKiemDemNuoc.getKeHoachKiemDemNuocId());
		thongTinKiemDemNuocImpl.setMaTinh(thongTinKiemDemNuoc.getMaTinh());
		thongTinKiemDemNuocImpl.setMaHuyen(thongTinKiemDemNuoc.getMaHuyen());
		thongTinKiemDemNuocImpl.setMaXa(thongTinKiemDemNuoc.getMaXa());
		thongTinKiemDemNuocImpl.setThonXom(thongTinKiemDemNuoc.getThonXom());
		thongTinKiemDemNuocImpl.setTenChuHo(thongTinKiemDemNuoc.getTenChuHo());
		thongTinKiemDemNuocImpl.setGioiTinhChuHo(thongTinKiemDemNuoc.getGioiTinhChuHo());
		thongTinKiemDemNuocImpl.setNgayKiemDem(thongTinKiemDemNuoc.getNgayKiemDem());
		thongTinKiemDemNuocImpl.setHoTenNguoiTraLoi(thongTinKiemDemNuoc.getHoTenNguoiTraLoi());
		thongTinKiemDemNuocImpl.setLaChuHoKhong(thongTinKiemDemNuoc.getLaChuHoKhong());
		thongTinKiemDemNuocImpl.setGioiTinhNguoiTraLoi(thongTinKiemDemNuoc.getGioiTinhNguoiTraLoi());
		thongTinKiemDemNuocImpl.setTenDanToc(thongTinKiemDemNuoc.getTenDanToc());
		thongTinKiemDemNuocImpl.setKinhDo(thongTinKiemDemNuoc.getKinhDo());
		thongTinKiemDemNuocImpl.setViDo(thongTinKiemDemNuoc.getViDo());
		thongTinKiemDemNuocImpl.setSoNguoiTrongHo(thongTinKiemDemNuoc.getSoNguoiTrongHo());
		thongTinKiemDemNuocImpl.setThanhPhanHoGd(thongTinKiemDemNuoc.getThanhPhanHoGd());
		thongTinKiemDemNuocImpl.setChuHoDungDs(thongTinKiemDemNuoc.getChuHoDungDs());
		thongTinKiemDemNuocImpl.setLyDoKhongDungDs(thongTinKiemDemNuoc.getLyDoKhongDungDs());
		thongTinKiemDemNuocImpl.setTenChuHoHienTai(thongTinKiemDemNuoc.getTenChuHoHienTai());
		thongTinKiemDemNuocImpl.setGioiTinhChuHoHienTai(thongTinKiemDemNuoc.getGioiTinhChuHoHienTai());
		thongTinKiemDemNuocImpl.setDauNoiChua(thongTinKiemDemNuoc.getDauNoiChua());
		thongTinKiemDemNuocImpl.setAnhDauNoi(thongTinKiemDemNuoc.getAnhDauNoi());
		thongTinKiemDemNuocImpl.setThoiGianDauHopDong(thongTinKiemDemNuoc.getThoiGianDauHopDong());
		thongTinKiemDemNuocImpl.setThoiGianDauNoiThucTe(thongTinKiemDemNuoc.getThoiGianDauNoiThucTe());
		thongTinKiemDemNuocImpl.setVoiNuocChayKhong(thongTinKiemDemNuoc.getVoiNuocChayKhong());
		thongTinKiemDemNuocImpl.setNuocTrongKhong(thongTinKiemDemNuoc.getNuocTrongKhong());
		thongTinKiemDemNuocImpl.setMauNuoc(thongTinKiemDemNuoc.getMauNuoc());
		thongTinKiemDemNuocImpl.setMuiNuoc(thongTinKiemDemNuoc.getMuiNuoc());
		thongTinKiemDemNuocImpl.setViNuoc(thongTinKiemDemNuoc.getViNuoc());
		thongTinKiemDemNuocImpl.setLoaiKhac(thongTinKiemDemNuoc.getLoaiKhac());
		thongTinKiemDemNuocImpl.setDaTungDucKhong(thongTinKiemDemNuoc.getDaTungDucKhong());
		thongTinKiemDemNuocImpl.setMauNuocTungCo(thongTinKiemDemNuoc.getMauNuocTungCo());
		thongTinKiemDemNuocImpl.setMuiNuocTungCo(thongTinKiemDemNuoc.getMuiNuocTungCo());
		thongTinKiemDemNuocImpl.setViNuocTungCo(thongTinKiemDemNuoc.getViNuocTungCo());
		thongTinKiemDemNuocImpl.setLoaiKhacTungCo(thongTinKiemDemNuoc.getLoaiKhacTungCo());
		thongTinKiemDemNuocImpl.setCoBeChuaNuoc(thongTinKiemDemNuoc.getCoBeChuaNuoc());
		thongTinKiemDemNuocImpl.setTinhTrangBe(thongTinKiemDemNuoc.getTinhTrangBe());
		thongTinKiemDemNuocImpl.setAnhBeChua(thongTinKiemDemNuoc.getAnhBeChua());
		thongTinKiemDemNuocImpl.setBiMatNuoc(thongTinKiemDemNuoc.getBiMatNuoc());
		thongTinKiemDemNuocImpl.setSoLanMatNuoc(thongTinKiemDemNuoc.getSoLanMatNuoc());
		thongTinKiemDemNuocImpl.setSoGioTrungBinh(thongTinKiemDemNuoc.getSoGioTrungBinh());
		thongTinKiemDemNuocImpl.setSoSeRyDongHo(thongTinKiemDemNuoc.getSoSeRyDongHo());
		thongTinKiemDemNuocImpl.setChiSoDongHo(thongTinKiemDemNuoc.getChiSoDongHo());
		thongTinKiemDemNuocImpl.setAnhDongHo(thongTinKiemDemNuoc.getAnhDongHo());
		thongTinKiemDemNuocImpl.setGhiChu(thongTinKiemDemNuoc.getGhiChu());
		thongTinKiemDemNuocImpl.setDaKetThucDieuTra(thongTinKiemDemNuoc.getDaKetThucDieuTra());
		thongTinKiemDemNuocImpl.setLyDoKhongHoanThanh(thongTinKiemDemNuoc.getLyDoKhongHoanThanh());
		thongTinKiemDemNuocImpl.setIMei(thongTinKiemDemNuoc.getIMei());
		thongTinKiemDemNuocImpl.setTaiKhoan(thongTinKiemDemNuoc.getTaiKhoan());
		thongTinKiemDemNuocImpl.setDanhGiaKiemDem(thongTinKiemDemNuoc.getDanhGiaKiemDem());

		return thongTinKiemDemNuocImpl;
	}

	/**
	 * Returns the thong tin kiem dem nuoc with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the thong tin kiem dem nuoc
	 * @return the thong tin kiem dem nuoc
	 * @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc findByPrimaryKey(Serializable primaryKey)
		throws NoSuchThongTinKiemDemNuocException, SystemException {
		ThongTinKiemDemNuoc thongTinKiemDemNuoc = fetchByPrimaryKey(primaryKey);

		if (thongTinKiemDemNuoc == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchThongTinKiemDemNuocException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return thongTinKiemDemNuoc;
	}

	/**
	 * Returns the thong tin kiem dem nuoc with the primary key or throws a {@link dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException} if it could not be found.
	 *
	 * @param id the primary key of the thong tin kiem dem nuoc
	 * @return the thong tin kiem dem nuoc
	 * @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc findByPrimaryKey(long id)
		throws NoSuchThongTinKiemDemNuocException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the thong tin kiem dem nuoc with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the thong tin kiem dem nuoc
	 * @return the thong tin kiem dem nuoc, or <code>null</code> if a thong tin kiem dem nuoc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ThongTinKiemDemNuoc thongTinKiemDemNuoc = (ThongTinKiemDemNuoc)EntityCacheUtil.getResult(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
				ThongTinKiemDemNuocImpl.class, primaryKey);

		if (thongTinKiemDemNuoc == _nullThongTinKiemDemNuoc) {
			return null;
		}

		if (thongTinKiemDemNuoc == null) {
			Session session = null;

			try {
				session = openSession();

				thongTinKiemDemNuoc = (ThongTinKiemDemNuoc)session.get(ThongTinKiemDemNuocImpl.class,
						primaryKey);

				if (thongTinKiemDemNuoc != null) {
					cacheResult(thongTinKiemDemNuoc);
				}
				else {
					EntityCacheUtil.putResult(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
						ThongTinKiemDemNuocImpl.class, primaryKey,
						_nullThongTinKiemDemNuoc);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ThongTinKiemDemNuocModelImpl.ENTITY_CACHE_ENABLED,
					ThongTinKiemDemNuocImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return thongTinKiemDemNuoc;
	}

	/**
	 * Returns the thong tin kiem dem nuoc with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the thong tin kiem dem nuoc
	 * @return the thong tin kiem dem nuoc, or <code>null</code> if a thong tin kiem dem nuoc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThongTinKiemDemNuoc fetchByPrimaryKey(long id)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the thong tin kiem dem nuocs.
	 *
	 * @return the thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThongTinKiemDemNuoc> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the thong tin kiem dem nuocs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of thong tin kiem dem nuocs
	 * @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	 * @return the range of thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThongTinKiemDemNuoc> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the thong tin kiem dem nuocs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of thong tin kiem dem nuocs
	 * @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThongTinKiemDemNuoc> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ThongTinKiemDemNuoc> list = (List<ThongTinKiemDemNuoc>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_THONGTINKIEMDEMNUOC);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_THONGTINKIEMDEMNUOC;

				if (pagination) {
					sql = sql.concat(ThongTinKiemDemNuocModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ThongTinKiemDemNuoc>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ThongTinKiemDemNuoc>(list);
				}
				else {
					list = (List<ThongTinKiemDemNuoc>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the thong tin kiem dem nuocs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ThongTinKiemDemNuoc thongTinKiemDemNuoc : findAll()) {
			remove(thongTinKiemDemNuoc);
		}
	}

	/**
	 * Returns the number of thong tin kiem dem nuocs.
	 *
	 * @return the number of thong tin kiem dem nuocs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_THONGTINKIEMDEMNUOC);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the thong tin kiem dem nuoc persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ThongTinKiemDemNuoc>> listenersList = new ArrayList<ModelListener<ThongTinKiemDemNuoc>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ThongTinKiemDemNuoc>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ThongTinKiemDemNuocImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_THONGTINKIEMDEMNUOC = "SELECT thongTinKiemDemNuoc FROM ThongTinKiemDemNuoc thongTinKiemDemNuoc";
	private static final String _SQL_SELECT_THONGTINKIEMDEMNUOC_WHERE = "SELECT thongTinKiemDemNuoc FROM ThongTinKiemDemNuoc thongTinKiemDemNuoc WHERE ";
	private static final String _SQL_COUNT_THONGTINKIEMDEMNUOC = "SELECT COUNT(thongTinKiemDemNuoc) FROM ThongTinKiemDemNuoc thongTinKiemDemNuoc";
	private static final String _SQL_COUNT_THONGTINKIEMDEMNUOC_WHERE = "SELECT COUNT(thongTinKiemDemNuoc) FROM ThongTinKiemDemNuoc thongTinKiemDemNuoc WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "thongTinKiemDemNuoc.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ThongTinKiemDemNuoc exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ThongTinKiemDemNuoc exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ThongTinKiemDemNuocPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"dauNoiNuocId", "keHoachKiemDemNuocId", "maTinh", "maHuyen",
				"maXa", "thonXom", "tenChuHo", "gioiTinhChuHo", "ngayKiemDem",
				"hoTenNguoiTraLoi", "laChuHoKhong", "gioiTinhNguoiTraLoi",
				"tenDanToc", "kinhDo", "viDo", "soNguoiTrongHo", "thanhPhanHoGd",
				"chuHoDungDs", "lyDoKhongDungDs", "tenChuHoHienTai",
				"gioiTinhChuHoHienTai", "dauNoiChua", "anhDauNoi",
				"thoiGianDauHopDong", "thoiGianDauNoiThucTe", "voiNuocChayKhong",
				"nuocTrongKhong", "mauNuoc", "muiNuoc", "viNuoc", "loaiKhac",
				"daTungDucKhong", "mauNuocTungCo", "muiNuocTungCo",
				"viNuocTungCo", "loaiKhacTungCo", "coBeChuaNuoc", "tinhTrangBe",
				"anhBeChua", "biMatNuoc", "soLanMatNuoc", "soGioTrungBinh",
				"soSeRyDongHo", "chiSoDongHo", "anhDongHo", "ghiChu",
				"daKetThucDieuTra", "lyDoKhongHoanThanh", "iMei", "taiKhoan",
				"danhGiaKiemDem"
			});
	private static ThongTinKiemDemNuoc _nullThongTinKiemDemNuoc = new ThongTinKiemDemNuocImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ThongTinKiemDemNuoc> toCacheModel() {
				return _nullThongTinKiemDemNuocCacheModel;
			}
		};

	private static CacheModel<ThongTinKiemDemNuoc> _nullThongTinKiemDemNuocCacheModel =
		new CacheModel<ThongTinKiemDemNuoc>() {
			@Override
			public ThongTinKiemDemNuoc toEntityModel() {
				return _nullThongTinKiemDemNuoc;
			}
		};
}