/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc;

/**
 * The persistence interface for the thong tin kiem dem nuoc service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author quangtrung
 * @see ThongTinKiemDemNuocPersistenceImpl
 * @see ThongTinKiemDemNuocUtil
 * @generated
 */
public interface ThongTinKiemDemNuocPersistence extends BasePersistence<ThongTinKiemDemNuoc> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ThongTinKiemDemNuocUtil} to access the thong tin kiem dem nuoc persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @return the matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findByD_dauNoiNuocId(
		long dauNoiNuocId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @return the range of matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findByD_dauNoiNuocId(
		long dauNoiNuocId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findByD_dauNoiNuocId(
		long dauNoiNuocId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc findByD_dauNoiNuocId_First(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException;

	/**
	* Returns the first thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching thong tin kiem dem nuoc, or <code>null</code> if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc fetchByD_dauNoiNuocId_First(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc findByD_dauNoiNuocId_Last(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException;

	/**
	* Returns the last thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching thong tin kiem dem nuoc, or <code>null</code> if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc fetchByD_dauNoiNuocId_Last(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the thong tin kiem dem nuocs before and after the current thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param id the primary key of the current thong tin kiem dem nuoc
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc[] findByD_dauNoiNuocId_PrevAndNext(
		long id, long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException;

	/**
	* Removes all the thong tin kiem dem nuocs where dauNoiNuocId = &#63; from the database.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByD_dauNoiNuocId(long dauNoiNuocId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @return the number of matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public int countByD_dauNoiNuocId(long dauNoiNuocId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the thong tin kiem dem nuoc in the entity cache if it is enabled.
	*
	* @param thongTinKiemDemNuoc the thong tin kiem dem nuoc
	*/
	public void cacheResult(
		dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc thongTinKiemDemNuoc);

	/**
	* Caches the thong tin kiem dem nuocs in the entity cache if it is enabled.
	*
	* @param thongTinKiemDemNuocs the thong tin kiem dem nuocs
	*/
	public void cacheResult(
		java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> thongTinKiemDemNuocs);

	/**
	* Creates a new thong tin kiem dem nuoc with the primary key. Does not add the thong tin kiem dem nuoc to the database.
	*
	* @param id the primary key for the new thong tin kiem dem nuoc
	* @return the new thong tin kiem dem nuoc
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc create(long id);

	/**
	* Removes the thong tin kiem dem nuoc with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the thong tin kiem dem nuoc
	* @return the thong tin kiem dem nuoc that was removed
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException;

	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc updateImpl(
		dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc thongTinKiemDemNuoc)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the thong tin kiem dem nuoc with the primary key or throws a {@link dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException} if it could not be found.
	*
	* @param id the primary key of the thong tin kiem dem nuoc
	* @return the thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException;

	/**
	* Returns the thong tin kiem dem nuoc with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the thong tin kiem dem nuoc
	* @return the thong tin kiem dem nuoc, or <code>null</code> if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the thong tin kiem dem nuocs.
	*
	* @return the thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the thong tin kiem dem nuocs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @return the range of thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the thong tin kiem dem nuocs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the thong tin kiem dem nuocs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of thong tin kiem dem nuocs.
	*
	* @return the number of thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}