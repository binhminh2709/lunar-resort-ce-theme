/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc;
import dtt.nuocsach.dao.sync.service.ThongTinKiemDemNuocLocalServiceUtil;

/**
 * The extended model base implementation for the ThongTinKiemDemNuoc service. Represents a row in the &quot;me_thongtinkiemdemnuoc&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link ThongTinKiemDemNuocImpl}.
 * </p>
 *
 * @author quangtrung
 * @see ThongTinKiemDemNuocImpl
 * @see dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc
 * @generated
 */
public abstract class ThongTinKiemDemNuocBaseImpl
	extends ThongTinKiemDemNuocModelImpl implements ThongTinKiemDemNuoc {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a thong tin kiem dem nuoc model instance should use the {@link ThongTinKiemDemNuoc} interface instead.
	 */
	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ThongTinKiemDemNuocLocalServiceUtil.addThongTinKiemDemNuoc(this);
		}
		else {
			ThongTinKiemDemNuocLocalServiceUtil.updateThongTinKiemDemNuoc(this);
		}
	}
}