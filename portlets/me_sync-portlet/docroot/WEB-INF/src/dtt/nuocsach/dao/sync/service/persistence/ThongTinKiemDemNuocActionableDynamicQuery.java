/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc;
import dtt.nuocsach.dao.sync.service.ThongTinKiemDemNuocLocalServiceUtil;

/**
 * @author quangtrung
 * @generated
 */
public abstract class ThongTinKiemDemNuocActionableDynamicQuery
	extends BaseActionableDynamicQuery {
	public ThongTinKiemDemNuocActionableDynamicQuery()
		throws SystemException {
		setBaseLocalService(ThongTinKiemDemNuocLocalServiceUtil.getService());
		setClass(ThongTinKiemDemNuoc.class);

		setClassLoader(dtt.nuocsach.dao.sync.service.ClpSerializer.class.getClassLoader());

		setPrimaryKeyPropertyName("id");
	}
}