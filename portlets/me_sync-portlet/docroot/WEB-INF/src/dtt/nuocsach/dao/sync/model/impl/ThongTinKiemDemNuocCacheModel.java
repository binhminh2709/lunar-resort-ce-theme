/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ThongTinKiemDemNuoc in entity cache.
 *
 * @author quangtrung
 * @see ThongTinKiemDemNuoc
 * @generated
 */
public class ThongTinKiemDemNuocCacheModel implements CacheModel<ThongTinKiemDemNuoc>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(105);

		sb.append("{id=");
		sb.append(id);
		sb.append(", dauNoiNuocId=");
		sb.append(dauNoiNuocId);
		sb.append(", keHoachKiemDemNuocId=");
		sb.append(keHoachKiemDemNuocId);
		sb.append(", maTinh=");
		sb.append(maTinh);
		sb.append(", maHuyen=");
		sb.append(maHuyen);
		sb.append(", maXa=");
		sb.append(maXa);
		sb.append(", thonXom=");
		sb.append(thonXom);
		sb.append(", tenChuHo=");
		sb.append(tenChuHo);
		sb.append(", gioiTinhChuHo=");
		sb.append(gioiTinhChuHo);
		sb.append(", ngayKiemDem=");
		sb.append(ngayKiemDem);
		sb.append(", hoTenNguoiTraLoi=");
		sb.append(hoTenNguoiTraLoi);
		sb.append(", laChuHoKhong=");
		sb.append(laChuHoKhong);
		sb.append(", gioiTinhNguoiTraLoi=");
		sb.append(gioiTinhNguoiTraLoi);
		sb.append(", tenDanToc=");
		sb.append(tenDanToc);
		sb.append(", kinhDo=");
		sb.append(kinhDo);
		sb.append(", viDo=");
		sb.append(viDo);
		sb.append(", soNguoiTrongHo=");
		sb.append(soNguoiTrongHo);
		sb.append(", thanhPhanHoGd=");
		sb.append(thanhPhanHoGd);
		sb.append(", chuHoDungDs=");
		sb.append(chuHoDungDs);
		sb.append(", lyDoKhongDungDs=");
		sb.append(lyDoKhongDungDs);
		sb.append(", tenChuHoHienTai=");
		sb.append(tenChuHoHienTai);
		sb.append(", gioiTinhChuHoHienTai=");
		sb.append(gioiTinhChuHoHienTai);
		sb.append(", dauNoiChua=");
		sb.append(dauNoiChua);
		sb.append(", anhDauNoi=");
		sb.append(anhDauNoi);
		sb.append(", thoiGianDauHopDong=");
		sb.append(thoiGianDauHopDong);
		sb.append(", thoiGianDauNoiThucTe=");
		sb.append(thoiGianDauNoiThucTe);
		sb.append(", voiNuocChayKhong=");
		sb.append(voiNuocChayKhong);
		sb.append(", nuocTrongKhong=");
		sb.append(nuocTrongKhong);
		sb.append(", mauNuoc=");
		sb.append(mauNuoc);
		sb.append(", muiNuoc=");
		sb.append(muiNuoc);
		sb.append(", viNuoc=");
		sb.append(viNuoc);
		sb.append(", loaiKhac=");
		sb.append(loaiKhac);
		sb.append(", daTungDucKhong=");
		sb.append(daTungDucKhong);
		sb.append(", mauNuocTungCo=");
		sb.append(mauNuocTungCo);
		sb.append(", muiNuocTungCo=");
		sb.append(muiNuocTungCo);
		sb.append(", viNuocTungCo=");
		sb.append(viNuocTungCo);
		sb.append(", loaiKhacTungCo=");
		sb.append(loaiKhacTungCo);
		sb.append(", coBeChuaNuoc=");
		sb.append(coBeChuaNuoc);
		sb.append(", tinhTrangBe=");
		sb.append(tinhTrangBe);
		sb.append(", anhBeChua=");
		sb.append(anhBeChua);
		sb.append(", biMatNuoc=");
		sb.append(biMatNuoc);
		sb.append(", soLanMatNuoc=");
		sb.append(soLanMatNuoc);
		sb.append(", soGioTrungBinh=");
		sb.append(soGioTrungBinh);
		sb.append(", soSeRyDongHo=");
		sb.append(soSeRyDongHo);
		sb.append(", chiSoDongHo=");
		sb.append(chiSoDongHo);
		sb.append(", anhDongHo=");
		sb.append(anhDongHo);
		sb.append(", ghiChu=");
		sb.append(ghiChu);
		sb.append(", daKetThucDieuTra=");
		sb.append(daKetThucDieuTra);
		sb.append(", lyDoKhongHoanThanh=");
		sb.append(lyDoKhongHoanThanh);
		sb.append(", iMei=");
		sb.append(iMei);
		sb.append(", taiKhoan=");
		sb.append(taiKhoan);
		sb.append(", danhGiaKiemDem=");
		sb.append(danhGiaKiemDem);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ThongTinKiemDemNuoc toEntityModel() {
		ThongTinKiemDemNuocImpl thongTinKiemDemNuocImpl = new ThongTinKiemDemNuocImpl();

		thongTinKiemDemNuocImpl.setId(id);
		thongTinKiemDemNuocImpl.setDauNoiNuocId(dauNoiNuocId);
		thongTinKiemDemNuocImpl.setKeHoachKiemDemNuocId(keHoachKiemDemNuocId);

		if (maTinh == null) {
			thongTinKiemDemNuocImpl.setMaTinh(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setMaTinh(maTinh);
		}

		if (maHuyen == null) {
			thongTinKiemDemNuocImpl.setMaHuyen(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setMaHuyen(maHuyen);
		}

		if (maXa == null) {
			thongTinKiemDemNuocImpl.setMaXa(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setMaXa(maXa);
		}

		if (thonXom == null) {
			thongTinKiemDemNuocImpl.setThonXom(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setThonXom(thonXom);
		}

		if (tenChuHo == null) {
			thongTinKiemDemNuocImpl.setTenChuHo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setTenChuHo(tenChuHo);
		}

		thongTinKiemDemNuocImpl.setGioiTinhChuHo(gioiTinhChuHo);

		if (ngayKiemDem == Long.MIN_VALUE) {
			thongTinKiemDemNuocImpl.setNgayKiemDem(null);
		}
		else {
			thongTinKiemDemNuocImpl.setNgayKiemDem(new Date(ngayKiemDem));
		}

		if (hoTenNguoiTraLoi == null) {
			thongTinKiemDemNuocImpl.setHoTenNguoiTraLoi(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setHoTenNguoiTraLoi(hoTenNguoiTraLoi);
		}

		thongTinKiemDemNuocImpl.setLaChuHoKhong(laChuHoKhong);

		if (gioiTinhNguoiTraLoi == null) {
			thongTinKiemDemNuocImpl.setGioiTinhNguoiTraLoi(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setGioiTinhNguoiTraLoi(gioiTinhNguoiTraLoi);
		}

		if (tenDanToc == null) {
			thongTinKiemDemNuocImpl.setTenDanToc(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setTenDanToc(tenDanToc);
		}

		if (kinhDo == null) {
			thongTinKiemDemNuocImpl.setKinhDo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setKinhDo(kinhDo);
		}

		if (viDo == null) {
			thongTinKiemDemNuocImpl.setViDo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setViDo(viDo);
		}

		thongTinKiemDemNuocImpl.setSoNguoiTrongHo(soNguoiTrongHo);
		thongTinKiemDemNuocImpl.setThanhPhanHoGd(thanhPhanHoGd);
		thongTinKiemDemNuocImpl.setChuHoDungDs(chuHoDungDs);

		if (lyDoKhongDungDs == null) {
			thongTinKiemDemNuocImpl.setLyDoKhongDungDs(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setLyDoKhongDungDs(lyDoKhongDungDs);
		}

		if (tenChuHoHienTai == null) {
			thongTinKiemDemNuocImpl.setTenChuHoHienTai(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setTenChuHoHienTai(tenChuHoHienTai);
		}

		thongTinKiemDemNuocImpl.setGioiTinhChuHoHienTai(gioiTinhChuHoHienTai);
		thongTinKiemDemNuocImpl.setDauNoiChua(dauNoiChua);

		if (anhDauNoi == null) {
			thongTinKiemDemNuocImpl.setAnhDauNoi(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setAnhDauNoi(anhDauNoi);
		}

		if (thoiGianDauHopDong == Long.MIN_VALUE) {
			thongTinKiemDemNuocImpl.setThoiGianDauHopDong(null);
		}
		else {
			thongTinKiemDemNuocImpl.setThoiGianDauHopDong(new Date(
					thoiGianDauHopDong));
		}

		if (thoiGianDauNoiThucTe == Long.MIN_VALUE) {
			thongTinKiemDemNuocImpl.setThoiGianDauNoiThucTe(null);
		}
		else {
			thongTinKiemDemNuocImpl.setThoiGianDauNoiThucTe(new Date(
					thoiGianDauNoiThucTe));
		}

		thongTinKiemDemNuocImpl.setVoiNuocChayKhong(voiNuocChayKhong);
		thongTinKiemDemNuocImpl.setNuocTrongKhong(nuocTrongKhong);

		if (mauNuoc == null) {
			thongTinKiemDemNuocImpl.setMauNuoc(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setMauNuoc(mauNuoc);
		}

		if (muiNuoc == null) {
			thongTinKiemDemNuocImpl.setMuiNuoc(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setMuiNuoc(muiNuoc);
		}

		if (viNuoc == null) {
			thongTinKiemDemNuocImpl.setViNuoc(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setViNuoc(viNuoc);
		}

		if (loaiKhac == null) {
			thongTinKiemDemNuocImpl.setLoaiKhac(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setLoaiKhac(loaiKhac);
		}

		thongTinKiemDemNuocImpl.setDaTungDucKhong(daTungDucKhong);

		if (mauNuocTungCo == null) {
			thongTinKiemDemNuocImpl.setMauNuocTungCo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setMauNuocTungCo(mauNuocTungCo);
		}

		if (muiNuocTungCo == null) {
			thongTinKiemDemNuocImpl.setMuiNuocTungCo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setMuiNuocTungCo(muiNuocTungCo);
		}

		if (viNuocTungCo == null) {
			thongTinKiemDemNuocImpl.setViNuocTungCo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setViNuocTungCo(viNuocTungCo);
		}

		if (loaiKhacTungCo == null) {
			thongTinKiemDemNuocImpl.setLoaiKhacTungCo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setLoaiKhacTungCo(loaiKhacTungCo);
		}

		thongTinKiemDemNuocImpl.setCoBeChuaNuoc(coBeChuaNuoc);
		thongTinKiemDemNuocImpl.setTinhTrangBe(tinhTrangBe);

		if (anhBeChua == null) {
			thongTinKiemDemNuocImpl.setAnhBeChua(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setAnhBeChua(anhBeChua);
		}

		thongTinKiemDemNuocImpl.setBiMatNuoc(biMatNuoc);
		thongTinKiemDemNuocImpl.setSoLanMatNuoc(soLanMatNuoc);
		thongTinKiemDemNuocImpl.setSoGioTrungBinh(soGioTrungBinh);

		if (soSeRyDongHo == null) {
			thongTinKiemDemNuocImpl.setSoSeRyDongHo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setSoSeRyDongHo(soSeRyDongHo);
		}

		if (chiSoDongHo == null) {
			thongTinKiemDemNuocImpl.setChiSoDongHo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setChiSoDongHo(chiSoDongHo);
		}

		if (anhDongHo == null) {
			thongTinKiemDemNuocImpl.setAnhDongHo(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setAnhDongHo(anhDongHo);
		}

		if (ghiChu == null) {
			thongTinKiemDemNuocImpl.setGhiChu(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setGhiChu(ghiChu);
		}

		thongTinKiemDemNuocImpl.setDaKetThucDieuTra(daKetThucDieuTra);

		if (lyDoKhongHoanThanh == null) {
			thongTinKiemDemNuocImpl.setLyDoKhongHoanThanh(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setLyDoKhongHoanThanh(lyDoKhongHoanThanh);
		}

		if (iMei == null) {
			thongTinKiemDemNuocImpl.setIMei(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setIMei(iMei);
		}

		if (taiKhoan == null) {
			thongTinKiemDemNuocImpl.setTaiKhoan(StringPool.BLANK);
		}
		else {
			thongTinKiemDemNuocImpl.setTaiKhoan(taiKhoan);
		}

		thongTinKiemDemNuocImpl.setDanhGiaKiemDem(danhGiaKiemDem);

		thongTinKiemDemNuocImpl.resetOriginalValues();

		return thongTinKiemDemNuocImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		dauNoiNuocId = objectInput.readLong();
		keHoachKiemDemNuocId = objectInput.readInt();
		maTinh = objectInput.readUTF();
		maHuyen = objectInput.readUTF();
		maXa = objectInput.readUTF();
		thonXom = objectInput.readUTF();
		tenChuHo = objectInput.readUTF();
		gioiTinhChuHo = objectInput.readInt();
		ngayKiemDem = objectInput.readLong();
		hoTenNguoiTraLoi = objectInput.readUTF();
		laChuHoKhong = objectInput.readInt();
		gioiTinhNguoiTraLoi = objectInput.readUTF();
		tenDanToc = objectInput.readUTF();
		kinhDo = objectInput.readUTF();
		viDo = objectInput.readUTF();
		soNguoiTrongHo = objectInput.readInt();
		thanhPhanHoGd = objectInput.readInt();
		chuHoDungDs = objectInput.readInt();
		lyDoKhongDungDs = objectInput.readUTF();
		tenChuHoHienTai = objectInput.readUTF();
		gioiTinhChuHoHienTai = objectInput.readInt();
		dauNoiChua = objectInput.readInt();
		anhDauNoi = objectInput.readUTF();
		thoiGianDauHopDong = objectInput.readLong();
		thoiGianDauNoiThucTe = objectInput.readLong();
		voiNuocChayKhong = objectInput.readInt();
		nuocTrongKhong = objectInput.readInt();
		mauNuoc = objectInput.readUTF();
		muiNuoc = objectInput.readUTF();
		viNuoc = objectInput.readUTF();
		loaiKhac = objectInput.readUTF();
		daTungDucKhong = objectInput.readInt();
		mauNuocTungCo = objectInput.readUTF();
		muiNuocTungCo = objectInput.readUTF();
		viNuocTungCo = objectInput.readUTF();
		loaiKhacTungCo = objectInput.readUTF();
		coBeChuaNuoc = objectInput.readInt();
		tinhTrangBe = objectInput.readInt();
		anhBeChua = objectInput.readUTF();
		biMatNuoc = objectInput.readInt();
		soLanMatNuoc = objectInput.readInt();
		soGioTrungBinh = objectInput.readInt();
		soSeRyDongHo = objectInput.readUTF();
		chiSoDongHo = objectInput.readUTF();
		anhDongHo = objectInput.readUTF();
		ghiChu = objectInput.readUTF();
		daKetThucDieuTra = objectInput.readInt();
		lyDoKhongHoanThanh = objectInput.readUTF();
		iMei = objectInput.readUTF();
		taiKhoan = objectInput.readUTF();
		danhGiaKiemDem = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(dauNoiNuocId);
		objectOutput.writeInt(keHoachKiemDemNuocId);

		if (maTinh == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(maTinh);
		}

		if (maHuyen == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(maHuyen);
		}

		if (maXa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(maXa);
		}

		if (thonXom == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(thonXom);
		}

		if (tenChuHo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tenChuHo);
		}

		objectOutput.writeInt(gioiTinhChuHo);
		objectOutput.writeLong(ngayKiemDem);

		if (hoTenNguoiTraLoi == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(hoTenNguoiTraLoi);
		}

		objectOutput.writeInt(laChuHoKhong);

		if (gioiTinhNguoiTraLoi == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(gioiTinhNguoiTraLoi);
		}

		if (tenDanToc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tenDanToc);
		}

		if (kinhDo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(kinhDo);
		}

		if (viDo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(viDo);
		}

		objectOutput.writeInt(soNguoiTrongHo);
		objectOutput.writeInt(thanhPhanHoGd);
		objectOutput.writeInt(chuHoDungDs);

		if (lyDoKhongDungDs == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lyDoKhongDungDs);
		}

		if (tenChuHoHienTai == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tenChuHoHienTai);
		}

		objectOutput.writeInt(gioiTinhChuHoHienTai);
		objectOutput.writeInt(dauNoiChua);

		if (anhDauNoi == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(anhDauNoi);
		}

		objectOutput.writeLong(thoiGianDauHopDong);
		objectOutput.writeLong(thoiGianDauNoiThucTe);
		objectOutput.writeInt(voiNuocChayKhong);
		objectOutput.writeInt(nuocTrongKhong);

		if (mauNuoc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mauNuoc);
		}

		if (muiNuoc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(muiNuoc);
		}

		if (viNuoc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(viNuoc);
		}

		if (loaiKhac == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(loaiKhac);
		}

		objectOutput.writeInt(daTungDucKhong);

		if (mauNuocTungCo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mauNuocTungCo);
		}

		if (muiNuocTungCo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(muiNuocTungCo);
		}

		if (viNuocTungCo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(viNuocTungCo);
		}

		if (loaiKhacTungCo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(loaiKhacTungCo);
		}

		objectOutput.writeInt(coBeChuaNuoc);
		objectOutput.writeInt(tinhTrangBe);

		if (anhBeChua == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(anhBeChua);
		}

		objectOutput.writeInt(biMatNuoc);
		objectOutput.writeInt(soLanMatNuoc);
		objectOutput.writeInt(soGioTrungBinh);

		if (soSeRyDongHo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(soSeRyDongHo);
		}

		if (chiSoDongHo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(chiSoDongHo);
		}

		if (anhDongHo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(anhDongHo);
		}

		if (ghiChu == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ghiChu);
		}

		objectOutput.writeInt(daKetThucDieuTra);

		if (lyDoKhongHoanThanh == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lyDoKhongHoanThanh);
		}

		if (iMei == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(iMei);
		}

		if (taiKhoan == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(taiKhoan);
		}

		objectOutput.writeInt(danhGiaKiemDem);
	}

	public long id;
	public long dauNoiNuocId;
	public int keHoachKiemDemNuocId;
	public String maTinh;
	public String maHuyen;
	public String maXa;
	public String thonXom;
	public String tenChuHo;
	public int gioiTinhChuHo;
	public long ngayKiemDem;
	public String hoTenNguoiTraLoi;
	public int laChuHoKhong;
	public String gioiTinhNguoiTraLoi;
	public String tenDanToc;
	public String kinhDo;
	public String viDo;
	public int soNguoiTrongHo;
	public int thanhPhanHoGd;
	public int chuHoDungDs;
	public String lyDoKhongDungDs;
	public String tenChuHoHienTai;
	public int gioiTinhChuHoHienTai;
	public int dauNoiChua;
	public String anhDauNoi;
	public long thoiGianDauHopDong;
	public long thoiGianDauNoiThucTe;
	public int voiNuocChayKhong;
	public int nuocTrongKhong;
	public String mauNuoc;
	public String muiNuoc;
	public String viNuoc;
	public String loaiKhac;
	public int daTungDucKhong;
	public String mauNuocTungCo;
	public String muiNuocTungCo;
	public String viNuocTungCo;
	public String loaiKhacTungCo;
	public int coBeChuaNuoc;
	public int tinhTrangBe;
	public String anhBeChua;
	public int biMatNuoc;
	public int soLanMatNuoc;
	public int soGioTrungBinh;
	public String soSeRyDongHo;
	public String chiSoDongHo;
	public String anhDongHo;
	public String ghiChu;
	public int daKetThucDieuTra;
	public String lyDoKhongHoanThanh;
	public String iMei;
	public String taiKhoan;
	public int danhGiaKiemDem;
}