/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ThongTinKiemDemNuoc}.
 * </p>
 *
 * @author quangtrung
 * @see ThongTinKiemDemNuoc
 * @generated
 */
public class ThongTinKiemDemNuocWrapper implements ThongTinKiemDemNuoc,
	ModelWrapper<ThongTinKiemDemNuoc> {
	public ThongTinKiemDemNuocWrapper(ThongTinKiemDemNuoc thongTinKiemDemNuoc) {
		_thongTinKiemDemNuoc = thongTinKiemDemNuoc;
	}

	@Override
	public Class<?> getModelClass() {
		return ThongTinKiemDemNuoc.class;
	}

	@Override
	public String getModelClassName() {
		return ThongTinKiemDemNuoc.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("dauNoiNuocId", getDauNoiNuocId());
		attributes.put("keHoachKiemDemNuocId", getKeHoachKiemDemNuocId());
		attributes.put("maTinh", getMaTinh());
		attributes.put("maHuyen", getMaHuyen());
		attributes.put("maXa", getMaXa());
		attributes.put("thonXom", getThonXom());
		attributes.put("tenChuHo", getTenChuHo());
		attributes.put("gioiTinhChuHo", getGioiTinhChuHo());
		attributes.put("ngayKiemDem", getNgayKiemDem());
		attributes.put("hoTenNguoiTraLoi", getHoTenNguoiTraLoi());
		attributes.put("laChuHoKhong", getLaChuHoKhong());
		attributes.put("gioiTinhNguoiTraLoi", getGioiTinhNguoiTraLoi());
		attributes.put("tenDanToc", getTenDanToc());
		attributes.put("kinhDo", getKinhDo());
		attributes.put("viDo", getViDo());
		attributes.put("soNguoiTrongHo", getSoNguoiTrongHo());
		attributes.put("thanhPhanHoGd", getThanhPhanHoGd());
		attributes.put("chuHoDungDs", getChuHoDungDs());
		attributes.put("lyDoKhongDungDs", getLyDoKhongDungDs());
		attributes.put("tenChuHoHienTai", getTenChuHoHienTai());
		attributes.put("gioiTinhChuHoHienTai", getGioiTinhChuHoHienTai());
		attributes.put("dauNoiChua", getDauNoiChua());
		attributes.put("anhDauNoi", getAnhDauNoi());
		attributes.put("thoiGianDauHopDong", getThoiGianDauHopDong());
		attributes.put("thoiGianDauNoiThucTe", getThoiGianDauNoiThucTe());
		attributes.put("voiNuocChayKhong", getVoiNuocChayKhong());
		attributes.put("nuocTrongKhong", getNuocTrongKhong());
		attributes.put("mauNuoc", getMauNuoc());
		attributes.put("muiNuoc", getMuiNuoc());
		attributes.put("viNuoc", getViNuoc());
		attributes.put("loaiKhac", getLoaiKhac());
		attributes.put("daTungDucKhong", getDaTungDucKhong());
		attributes.put("mauNuocTungCo", getMauNuocTungCo());
		attributes.put("muiNuocTungCo", getMuiNuocTungCo());
		attributes.put("viNuocTungCo", getViNuocTungCo());
		attributes.put("loaiKhacTungCo", getLoaiKhacTungCo());
		attributes.put("coBeChuaNuoc", getCoBeChuaNuoc());
		attributes.put("tinhTrangBe", getTinhTrangBe());
		attributes.put("anhBeChua", getAnhBeChua());
		attributes.put("biMatNuoc", getBiMatNuoc());
		attributes.put("soLanMatNuoc", getSoLanMatNuoc());
		attributes.put("soGioTrungBinh", getSoGioTrungBinh());
		attributes.put("soSeRyDongHo", getSoSeRyDongHo());
		attributes.put("chiSoDongHo", getChiSoDongHo());
		attributes.put("anhDongHo", getAnhDongHo());
		attributes.put("ghiChu", getGhiChu());
		attributes.put("daKetThucDieuTra", getDaKetThucDieuTra());
		attributes.put("lyDoKhongHoanThanh", getLyDoKhongHoanThanh());
		attributes.put("iMei", getIMei());
		attributes.put("taiKhoan", getTaiKhoan());
		attributes.put("danhGiaKiemDem", getDanhGiaKiemDem());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long dauNoiNuocId = (Long)attributes.get("dauNoiNuocId");

		if (dauNoiNuocId != null) {
			setDauNoiNuocId(dauNoiNuocId);
		}

		Integer keHoachKiemDemNuocId = (Integer)attributes.get(
				"keHoachKiemDemNuocId");

		if (keHoachKiemDemNuocId != null) {
			setKeHoachKiemDemNuocId(keHoachKiemDemNuocId);
		}

		String maTinh = (String)attributes.get("maTinh");

		if (maTinh != null) {
			setMaTinh(maTinh);
		}

		String maHuyen = (String)attributes.get("maHuyen");

		if (maHuyen != null) {
			setMaHuyen(maHuyen);
		}

		String maXa = (String)attributes.get("maXa");

		if (maXa != null) {
			setMaXa(maXa);
		}

		String thonXom = (String)attributes.get("thonXom");

		if (thonXom != null) {
			setThonXom(thonXom);
		}

		String tenChuHo = (String)attributes.get("tenChuHo");

		if (tenChuHo != null) {
			setTenChuHo(tenChuHo);
		}

		Integer gioiTinhChuHo = (Integer)attributes.get("gioiTinhChuHo");

		if (gioiTinhChuHo != null) {
			setGioiTinhChuHo(gioiTinhChuHo);
		}

		Date ngayKiemDem = (Date)attributes.get("ngayKiemDem");

		if (ngayKiemDem != null) {
			setNgayKiemDem(ngayKiemDem);
		}

		String hoTenNguoiTraLoi = (String)attributes.get("hoTenNguoiTraLoi");

		if (hoTenNguoiTraLoi != null) {
			setHoTenNguoiTraLoi(hoTenNguoiTraLoi);
		}

		Integer laChuHoKhong = (Integer)attributes.get("laChuHoKhong");

		if (laChuHoKhong != null) {
			setLaChuHoKhong(laChuHoKhong);
		}

		String gioiTinhNguoiTraLoi = (String)attributes.get(
				"gioiTinhNguoiTraLoi");

		if (gioiTinhNguoiTraLoi != null) {
			setGioiTinhNguoiTraLoi(gioiTinhNguoiTraLoi);
		}

		String tenDanToc = (String)attributes.get("tenDanToc");

		if (tenDanToc != null) {
			setTenDanToc(tenDanToc);
		}

		String kinhDo = (String)attributes.get("kinhDo");

		if (kinhDo != null) {
			setKinhDo(kinhDo);
		}

		String viDo = (String)attributes.get("viDo");

		if (viDo != null) {
			setViDo(viDo);
		}

		Integer soNguoiTrongHo = (Integer)attributes.get("soNguoiTrongHo");

		if (soNguoiTrongHo != null) {
			setSoNguoiTrongHo(soNguoiTrongHo);
		}

		Integer thanhPhanHoGd = (Integer)attributes.get("thanhPhanHoGd");

		if (thanhPhanHoGd != null) {
			setThanhPhanHoGd(thanhPhanHoGd);
		}

		Integer chuHoDungDs = (Integer)attributes.get("chuHoDungDs");

		if (chuHoDungDs != null) {
			setChuHoDungDs(chuHoDungDs);
		}

		String lyDoKhongDungDs = (String)attributes.get("lyDoKhongDungDs");

		if (lyDoKhongDungDs != null) {
			setLyDoKhongDungDs(lyDoKhongDungDs);
		}

		String tenChuHoHienTai = (String)attributes.get("tenChuHoHienTai");

		if (tenChuHoHienTai != null) {
			setTenChuHoHienTai(tenChuHoHienTai);
		}

		Integer gioiTinhChuHoHienTai = (Integer)attributes.get(
				"gioiTinhChuHoHienTai");

		if (gioiTinhChuHoHienTai != null) {
			setGioiTinhChuHoHienTai(gioiTinhChuHoHienTai);
		}

		Integer dauNoiChua = (Integer)attributes.get("dauNoiChua");

		if (dauNoiChua != null) {
			setDauNoiChua(dauNoiChua);
		}

		String anhDauNoi = (String)attributes.get("anhDauNoi");

		if (anhDauNoi != null) {
			setAnhDauNoi(anhDauNoi);
		}

		Date thoiGianDauHopDong = (Date)attributes.get("thoiGianDauHopDong");

		if (thoiGianDauHopDong != null) {
			setThoiGianDauHopDong(thoiGianDauHopDong);
		}

		Date thoiGianDauNoiThucTe = (Date)attributes.get("thoiGianDauNoiThucTe");

		if (thoiGianDauNoiThucTe != null) {
			setThoiGianDauNoiThucTe(thoiGianDauNoiThucTe);
		}

		Integer voiNuocChayKhong = (Integer)attributes.get("voiNuocChayKhong");

		if (voiNuocChayKhong != null) {
			setVoiNuocChayKhong(voiNuocChayKhong);
		}

		Integer nuocTrongKhong = (Integer)attributes.get("nuocTrongKhong");

		if (nuocTrongKhong != null) {
			setNuocTrongKhong(nuocTrongKhong);
		}

		String mauNuoc = (String)attributes.get("mauNuoc");

		if (mauNuoc != null) {
			setMauNuoc(mauNuoc);
		}

		String muiNuoc = (String)attributes.get("muiNuoc");

		if (muiNuoc != null) {
			setMuiNuoc(muiNuoc);
		}

		String viNuoc = (String)attributes.get("viNuoc");

		if (viNuoc != null) {
			setViNuoc(viNuoc);
		}

		String loaiKhac = (String)attributes.get("loaiKhac");

		if (loaiKhac != null) {
			setLoaiKhac(loaiKhac);
		}

		Integer daTungDucKhong = (Integer)attributes.get("daTungDucKhong");

		if (daTungDucKhong != null) {
			setDaTungDucKhong(daTungDucKhong);
		}

		String mauNuocTungCo = (String)attributes.get("mauNuocTungCo");

		if (mauNuocTungCo != null) {
			setMauNuocTungCo(mauNuocTungCo);
		}

		String muiNuocTungCo = (String)attributes.get("muiNuocTungCo");

		if (muiNuocTungCo != null) {
			setMuiNuocTungCo(muiNuocTungCo);
		}

		String viNuocTungCo = (String)attributes.get("viNuocTungCo");

		if (viNuocTungCo != null) {
			setViNuocTungCo(viNuocTungCo);
		}

		String loaiKhacTungCo = (String)attributes.get("loaiKhacTungCo");

		if (loaiKhacTungCo != null) {
			setLoaiKhacTungCo(loaiKhacTungCo);
		}

		Integer coBeChuaNuoc = (Integer)attributes.get("coBeChuaNuoc");

		if (coBeChuaNuoc != null) {
			setCoBeChuaNuoc(coBeChuaNuoc);
		}

		Integer tinhTrangBe = (Integer)attributes.get("tinhTrangBe");

		if (tinhTrangBe != null) {
			setTinhTrangBe(tinhTrangBe);
		}

		String anhBeChua = (String)attributes.get("anhBeChua");

		if (anhBeChua != null) {
			setAnhBeChua(anhBeChua);
		}

		Integer biMatNuoc = (Integer)attributes.get("biMatNuoc");

		if (biMatNuoc != null) {
			setBiMatNuoc(biMatNuoc);
		}

		Integer soLanMatNuoc = (Integer)attributes.get("soLanMatNuoc");

		if (soLanMatNuoc != null) {
			setSoLanMatNuoc(soLanMatNuoc);
		}

		Integer soGioTrungBinh = (Integer)attributes.get("soGioTrungBinh");

		if (soGioTrungBinh != null) {
			setSoGioTrungBinh(soGioTrungBinh);
		}

		String soSeRyDongHo = (String)attributes.get("soSeRyDongHo");

		if (soSeRyDongHo != null) {
			setSoSeRyDongHo(soSeRyDongHo);
		}

		String chiSoDongHo = (String)attributes.get("chiSoDongHo");

		if (chiSoDongHo != null) {
			setChiSoDongHo(chiSoDongHo);
		}

		String anhDongHo = (String)attributes.get("anhDongHo");

		if (anhDongHo != null) {
			setAnhDongHo(anhDongHo);
		}

		String ghiChu = (String)attributes.get("ghiChu");

		if (ghiChu != null) {
			setGhiChu(ghiChu);
		}

		Integer daKetThucDieuTra = (Integer)attributes.get("daKetThucDieuTra");

		if (daKetThucDieuTra != null) {
			setDaKetThucDieuTra(daKetThucDieuTra);
		}

		String lyDoKhongHoanThanh = (String)attributes.get("lyDoKhongHoanThanh");

		if (lyDoKhongHoanThanh != null) {
			setLyDoKhongHoanThanh(lyDoKhongHoanThanh);
		}

		String iMei = (String)attributes.get("iMei");

		if (iMei != null) {
			setIMei(iMei);
		}

		String taiKhoan = (String)attributes.get("taiKhoan");

		if (taiKhoan != null) {
			setTaiKhoan(taiKhoan);
		}

		Integer danhGiaKiemDem = (Integer)attributes.get("danhGiaKiemDem");

		if (danhGiaKiemDem != null) {
			setDanhGiaKiemDem(danhGiaKiemDem);
		}
	}

	/**
	* Returns the primary key of this thong tin kiem dem nuoc.
	*
	* @return the primary key of this thong tin kiem dem nuoc
	*/
	@Override
	public long getPrimaryKey() {
		return _thongTinKiemDemNuoc.getPrimaryKey();
	}

	/**
	* Sets the primary key of this thong tin kiem dem nuoc.
	*
	* @param primaryKey the primary key of this thong tin kiem dem nuoc
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_thongTinKiemDemNuoc.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this thong tin kiem dem nuoc.
	*
	* @return the ID of this thong tin kiem dem nuoc
	*/
	@Override
	public long getId() {
		return _thongTinKiemDemNuoc.getId();
	}

	/**
	* Sets the ID of this thong tin kiem dem nuoc.
	*
	* @param id the ID of this thong tin kiem dem nuoc
	*/
	@Override
	public void setId(long id) {
		_thongTinKiemDemNuoc.setId(id);
	}

	/**
	* Returns the dau noi nuoc ID of this thong tin kiem dem nuoc.
	*
	* @return the dau noi nuoc ID of this thong tin kiem dem nuoc
	*/
	@Override
	public long getDauNoiNuocId() {
		return _thongTinKiemDemNuoc.getDauNoiNuocId();
	}

	/**
	* Sets the dau noi nuoc ID of this thong tin kiem dem nuoc.
	*
	* @param dauNoiNuocId the dau noi nuoc ID of this thong tin kiem dem nuoc
	*/
	@Override
	public void setDauNoiNuocId(long dauNoiNuocId) {
		_thongTinKiemDemNuoc.setDauNoiNuocId(dauNoiNuocId);
	}

	/**
	* Returns the ke hoach kiem dem nuoc ID of this thong tin kiem dem nuoc.
	*
	* @return the ke hoach kiem dem nuoc ID of this thong tin kiem dem nuoc
	*/
	@Override
	public int getKeHoachKiemDemNuocId() {
		return _thongTinKiemDemNuoc.getKeHoachKiemDemNuocId();
	}

	/**
	* Sets the ke hoach kiem dem nuoc ID of this thong tin kiem dem nuoc.
	*
	* @param keHoachKiemDemNuocId the ke hoach kiem dem nuoc ID of this thong tin kiem dem nuoc
	*/
	@Override
	public void setKeHoachKiemDemNuocId(int keHoachKiemDemNuocId) {
		_thongTinKiemDemNuoc.setKeHoachKiemDemNuocId(keHoachKiemDemNuocId);
	}

	/**
	* Returns the ma tinh of this thong tin kiem dem nuoc.
	*
	* @return the ma tinh of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getMaTinh() {
		return _thongTinKiemDemNuoc.getMaTinh();
	}

	/**
	* Sets the ma tinh of this thong tin kiem dem nuoc.
	*
	* @param maTinh the ma tinh of this thong tin kiem dem nuoc
	*/
	@Override
	public void setMaTinh(java.lang.String maTinh) {
		_thongTinKiemDemNuoc.setMaTinh(maTinh);
	}

	/**
	* Returns the ma huyen of this thong tin kiem dem nuoc.
	*
	* @return the ma huyen of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getMaHuyen() {
		return _thongTinKiemDemNuoc.getMaHuyen();
	}

	/**
	* Sets the ma huyen of this thong tin kiem dem nuoc.
	*
	* @param maHuyen the ma huyen of this thong tin kiem dem nuoc
	*/
	@Override
	public void setMaHuyen(java.lang.String maHuyen) {
		_thongTinKiemDemNuoc.setMaHuyen(maHuyen);
	}

	/**
	* Returns the ma xa of this thong tin kiem dem nuoc.
	*
	* @return the ma xa of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getMaXa() {
		return _thongTinKiemDemNuoc.getMaXa();
	}

	/**
	* Sets the ma xa of this thong tin kiem dem nuoc.
	*
	* @param maXa the ma xa of this thong tin kiem dem nuoc
	*/
	@Override
	public void setMaXa(java.lang.String maXa) {
		_thongTinKiemDemNuoc.setMaXa(maXa);
	}

	/**
	* Returns the thon xom of this thong tin kiem dem nuoc.
	*
	* @return the thon xom of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getThonXom() {
		return _thongTinKiemDemNuoc.getThonXom();
	}

	/**
	* Sets the thon xom of this thong tin kiem dem nuoc.
	*
	* @param thonXom the thon xom of this thong tin kiem dem nuoc
	*/
	@Override
	public void setThonXom(java.lang.String thonXom) {
		_thongTinKiemDemNuoc.setThonXom(thonXom);
	}

	/**
	* Returns the ten chu ho of this thong tin kiem dem nuoc.
	*
	* @return the ten chu ho of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getTenChuHo() {
		return _thongTinKiemDemNuoc.getTenChuHo();
	}

	/**
	* Sets the ten chu ho of this thong tin kiem dem nuoc.
	*
	* @param tenChuHo the ten chu ho of this thong tin kiem dem nuoc
	*/
	@Override
	public void setTenChuHo(java.lang.String tenChuHo) {
		_thongTinKiemDemNuoc.setTenChuHo(tenChuHo);
	}

	/**
	* Returns the gioi tinh chu ho of this thong tin kiem dem nuoc.
	*
	* @return the gioi tinh chu ho of this thong tin kiem dem nuoc
	*/
	@Override
	public int getGioiTinhChuHo() {
		return _thongTinKiemDemNuoc.getGioiTinhChuHo();
	}

	/**
	* Sets the gioi tinh chu ho of this thong tin kiem dem nuoc.
	*
	* @param gioiTinhChuHo the gioi tinh chu ho of this thong tin kiem dem nuoc
	*/
	@Override
	public void setGioiTinhChuHo(int gioiTinhChuHo) {
		_thongTinKiemDemNuoc.setGioiTinhChuHo(gioiTinhChuHo);
	}

	/**
	* Returns the ngay kiem dem of this thong tin kiem dem nuoc.
	*
	* @return the ngay kiem dem of this thong tin kiem dem nuoc
	*/
	@Override
	public java.util.Date getNgayKiemDem() {
		return _thongTinKiemDemNuoc.getNgayKiemDem();
	}

	/**
	* Sets the ngay kiem dem of this thong tin kiem dem nuoc.
	*
	* @param ngayKiemDem the ngay kiem dem of this thong tin kiem dem nuoc
	*/
	@Override
	public void setNgayKiemDem(java.util.Date ngayKiemDem) {
		_thongTinKiemDemNuoc.setNgayKiemDem(ngayKiemDem);
	}

	/**
	* Returns the ho ten nguoi tra loi of this thong tin kiem dem nuoc.
	*
	* @return the ho ten nguoi tra loi of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getHoTenNguoiTraLoi() {
		return _thongTinKiemDemNuoc.getHoTenNguoiTraLoi();
	}

	/**
	* Sets the ho ten nguoi tra loi of this thong tin kiem dem nuoc.
	*
	* @param hoTenNguoiTraLoi the ho ten nguoi tra loi of this thong tin kiem dem nuoc
	*/
	@Override
	public void setHoTenNguoiTraLoi(java.lang.String hoTenNguoiTraLoi) {
		_thongTinKiemDemNuoc.setHoTenNguoiTraLoi(hoTenNguoiTraLoi);
	}

	/**
	* Returns the la chu ho khong of this thong tin kiem dem nuoc.
	*
	* @return the la chu ho khong of this thong tin kiem dem nuoc
	*/
	@Override
	public int getLaChuHoKhong() {
		return _thongTinKiemDemNuoc.getLaChuHoKhong();
	}

	/**
	* Sets the la chu ho khong of this thong tin kiem dem nuoc.
	*
	* @param laChuHoKhong the la chu ho khong of this thong tin kiem dem nuoc
	*/
	@Override
	public void setLaChuHoKhong(int laChuHoKhong) {
		_thongTinKiemDemNuoc.setLaChuHoKhong(laChuHoKhong);
	}

	/**
	* Returns the gioi tinh nguoi tra loi of this thong tin kiem dem nuoc.
	*
	* @return the gioi tinh nguoi tra loi of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getGioiTinhNguoiTraLoi() {
		return _thongTinKiemDemNuoc.getGioiTinhNguoiTraLoi();
	}

	/**
	* Sets the gioi tinh nguoi tra loi of this thong tin kiem dem nuoc.
	*
	* @param gioiTinhNguoiTraLoi the gioi tinh nguoi tra loi of this thong tin kiem dem nuoc
	*/
	@Override
	public void setGioiTinhNguoiTraLoi(java.lang.String gioiTinhNguoiTraLoi) {
		_thongTinKiemDemNuoc.setGioiTinhNguoiTraLoi(gioiTinhNguoiTraLoi);
	}

	/**
	* Returns the ten dan toc of this thong tin kiem dem nuoc.
	*
	* @return the ten dan toc of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getTenDanToc() {
		return _thongTinKiemDemNuoc.getTenDanToc();
	}

	/**
	* Sets the ten dan toc of this thong tin kiem dem nuoc.
	*
	* @param tenDanToc the ten dan toc of this thong tin kiem dem nuoc
	*/
	@Override
	public void setTenDanToc(java.lang.String tenDanToc) {
		_thongTinKiemDemNuoc.setTenDanToc(tenDanToc);
	}

	/**
	* Returns the kinh do of this thong tin kiem dem nuoc.
	*
	* @return the kinh do of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getKinhDo() {
		return _thongTinKiemDemNuoc.getKinhDo();
	}

	/**
	* Sets the kinh do of this thong tin kiem dem nuoc.
	*
	* @param kinhDo the kinh do of this thong tin kiem dem nuoc
	*/
	@Override
	public void setKinhDo(java.lang.String kinhDo) {
		_thongTinKiemDemNuoc.setKinhDo(kinhDo);
	}

	/**
	* Returns the vi do of this thong tin kiem dem nuoc.
	*
	* @return the vi do of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getViDo() {
		return _thongTinKiemDemNuoc.getViDo();
	}

	/**
	* Sets the vi do of this thong tin kiem dem nuoc.
	*
	* @param viDo the vi do of this thong tin kiem dem nuoc
	*/
	@Override
	public void setViDo(java.lang.String viDo) {
		_thongTinKiemDemNuoc.setViDo(viDo);
	}

	/**
	* Returns the so nguoi trong ho of this thong tin kiem dem nuoc.
	*
	* @return the so nguoi trong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public int getSoNguoiTrongHo() {
		return _thongTinKiemDemNuoc.getSoNguoiTrongHo();
	}

	/**
	* Sets the so nguoi trong ho of this thong tin kiem dem nuoc.
	*
	* @param soNguoiTrongHo the so nguoi trong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public void setSoNguoiTrongHo(int soNguoiTrongHo) {
		_thongTinKiemDemNuoc.setSoNguoiTrongHo(soNguoiTrongHo);
	}

	/**
	* Returns the thanh phan ho gd of this thong tin kiem dem nuoc.
	*
	* @return the thanh phan ho gd of this thong tin kiem dem nuoc
	*/
	@Override
	public int getThanhPhanHoGd() {
		return _thongTinKiemDemNuoc.getThanhPhanHoGd();
	}

	/**
	* Sets the thanh phan ho gd of this thong tin kiem dem nuoc.
	*
	* @param thanhPhanHoGd the thanh phan ho gd of this thong tin kiem dem nuoc
	*/
	@Override
	public void setThanhPhanHoGd(int thanhPhanHoGd) {
		_thongTinKiemDemNuoc.setThanhPhanHoGd(thanhPhanHoGd);
	}

	/**
	* Returns the chu ho dung ds of this thong tin kiem dem nuoc.
	*
	* @return the chu ho dung ds of this thong tin kiem dem nuoc
	*/
	@Override
	public int getChuHoDungDs() {
		return _thongTinKiemDemNuoc.getChuHoDungDs();
	}

	/**
	* Sets the chu ho dung ds of this thong tin kiem dem nuoc.
	*
	* @param chuHoDungDs the chu ho dung ds of this thong tin kiem dem nuoc
	*/
	@Override
	public void setChuHoDungDs(int chuHoDungDs) {
		_thongTinKiemDemNuoc.setChuHoDungDs(chuHoDungDs);
	}

	/**
	* Returns the ly do khong dung ds of this thong tin kiem dem nuoc.
	*
	* @return the ly do khong dung ds of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getLyDoKhongDungDs() {
		return _thongTinKiemDemNuoc.getLyDoKhongDungDs();
	}

	/**
	* Sets the ly do khong dung ds of this thong tin kiem dem nuoc.
	*
	* @param lyDoKhongDungDs the ly do khong dung ds of this thong tin kiem dem nuoc
	*/
	@Override
	public void setLyDoKhongDungDs(java.lang.String lyDoKhongDungDs) {
		_thongTinKiemDemNuoc.setLyDoKhongDungDs(lyDoKhongDungDs);
	}

	/**
	* Returns the ten chu ho hien tai of this thong tin kiem dem nuoc.
	*
	* @return the ten chu ho hien tai of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getTenChuHoHienTai() {
		return _thongTinKiemDemNuoc.getTenChuHoHienTai();
	}

	/**
	* Sets the ten chu ho hien tai of this thong tin kiem dem nuoc.
	*
	* @param tenChuHoHienTai the ten chu ho hien tai of this thong tin kiem dem nuoc
	*/
	@Override
	public void setTenChuHoHienTai(java.lang.String tenChuHoHienTai) {
		_thongTinKiemDemNuoc.setTenChuHoHienTai(tenChuHoHienTai);
	}

	/**
	* Returns the gioi tinh chu ho hien tai of this thong tin kiem dem nuoc.
	*
	* @return the gioi tinh chu ho hien tai of this thong tin kiem dem nuoc
	*/
	@Override
	public int getGioiTinhChuHoHienTai() {
		return _thongTinKiemDemNuoc.getGioiTinhChuHoHienTai();
	}

	/**
	* Sets the gioi tinh chu ho hien tai of this thong tin kiem dem nuoc.
	*
	* @param gioiTinhChuHoHienTai the gioi tinh chu ho hien tai of this thong tin kiem dem nuoc
	*/
	@Override
	public void setGioiTinhChuHoHienTai(int gioiTinhChuHoHienTai) {
		_thongTinKiemDemNuoc.setGioiTinhChuHoHienTai(gioiTinhChuHoHienTai);
	}

	/**
	* Returns the dau noi chua of this thong tin kiem dem nuoc.
	*
	* @return the dau noi chua of this thong tin kiem dem nuoc
	*/
	@Override
	public int getDauNoiChua() {
		return _thongTinKiemDemNuoc.getDauNoiChua();
	}

	/**
	* Sets the dau noi chua of this thong tin kiem dem nuoc.
	*
	* @param dauNoiChua the dau noi chua of this thong tin kiem dem nuoc
	*/
	@Override
	public void setDauNoiChua(int dauNoiChua) {
		_thongTinKiemDemNuoc.setDauNoiChua(dauNoiChua);
	}

	/**
	* Returns the anh dau noi of this thong tin kiem dem nuoc.
	*
	* @return the anh dau noi of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getAnhDauNoi() {
		return _thongTinKiemDemNuoc.getAnhDauNoi();
	}

	/**
	* Sets the anh dau noi of this thong tin kiem dem nuoc.
	*
	* @param anhDauNoi the anh dau noi of this thong tin kiem dem nuoc
	*/
	@Override
	public void setAnhDauNoi(java.lang.String anhDauNoi) {
		_thongTinKiemDemNuoc.setAnhDauNoi(anhDauNoi);
	}

	/**
	* Returns the thoi gian dau hop dong of this thong tin kiem dem nuoc.
	*
	* @return the thoi gian dau hop dong of this thong tin kiem dem nuoc
	*/
	@Override
	public java.util.Date getThoiGianDauHopDong() {
		return _thongTinKiemDemNuoc.getThoiGianDauHopDong();
	}

	/**
	* Sets the thoi gian dau hop dong of this thong tin kiem dem nuoc.
	*
	* @param thoiGianDauHopDong the thoi gian dau hop dong of this thong tin kiem dem nuoc
	*/
	@Override
	public void setThoiGianDauHopDong(java.util.Date thoiGianDauHopDong) {
		_thongTinKiemDemNuoc.setThoiGianDauHopDong(thoiGianDauHopDong);
	}

	/**
	* Returns the thoi gian dau noi thuc te of this thong tin kiem dem nuoc.
	*
	* @return the thoi gian dau noi thuc te of this thong tin kiem dem nuoc
	*/
	@Override
	public java.util.Date getThoiGianDauNoiThucTe() {
		return _thongTinKiemDemNuoc.getThoiGianDauNoiThucTe();
	}

	/**
	* Sets the thoi gian dau noi thuc te of this thong tin kiem dem nuoc.
	*
	* @param thoiGianDauNoiThucTe the thoi gian dau noi thuc te of this thong tin kiem dem nuoc
	*/
	@Override
	public void setThoiGianDauNoiThucTe(java.util.Date thoiGianDauNoiThucTe) {
		_thongTinKiemDemNuoc.setThoiGianDauNoiThucTe(thoiGianDauNoiThucTe);
	}

	/**
	* Returns the voi nuoc chay khong of this thong tin kiem dem nuoc.
	*
	* @return the voi nuoc chay khong of this thong tin kiem dem nuoc
	*/
	@Override
	public int getVoiNuocChayKhong() {
		return _thongTinKiemDemNuoc.getVoiNuocChayKhong();
	}

	/**
	* Sets the voi nuoc chay khong of this thong tin kiem dem nuoc.
	*
	* @param voiNuocChayKhong the voi nuoc chay khong of this thong tin kiem dem nuoc
	*/
	@Override
	public void setVoiNuocChayKhong(int voiNuocChayKhong) {
		_thongTinKiemDemNuoc.setVoiNuocChayKhong(voiNuocChayKhong);
	}

	/**
	* Returns the nuoc trong khong of this thong tin kiem dem nuoc.
	*
	* @return the nuoc trong khong of this thong tin kiem dem nuoc
	*/
	@Override
	public int getNuocTrongKhong() {
		return _thongTinKiemDemNuoc.getNuocTrongKhong();
	}

	/**
	* Sets the nuoc trong khong of this thong tin kiem dem nuoc.
	*
	* @param nuocTrongKhong the nuoc trong khong of this thong tin kiem dem nuoc
	*/
	@Override
	public void setNuocTrongKhong(int nuocTrongKhong) {
		_thongTinKiemDemNuoc.setNuocTrongKhong(nuocTrongKhong);
	}

	/**
	* Returns the mau nuoc of this thong tin kiem dem nuoc.
	*
	* @return the mau nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getMauNuoc() {
		return _thongTinKiemDemNuoc.getMauNuoc();
	}

	/**
	* Sets the mau nuoc of this thong tin kiem dem nuoc.
	*
	* @param mauNuoc the mau nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public void setMauNuoc(java.lang.String mauNuoc) {
		_thongTinKiemDemNuoc.setMauNuoc(mauNuoc);
	}

	/**
	* Returns the mui nuoc of this thong tin kiem dem nuoc.
	*
	* @return the mui nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getMuiNuoc() {
		return _thongTinKiemDemNuoc.getMuiNuoc();
	}

	/**
	* Sets the mui nuoc of this thong tin kiem dem nuoc.
	*
	* @param muiNuoc the mui nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public void setMuiNuoc(java.lang.String muiNuoc) {
		_thongTinKiemDemNuoc.setMuiNuoc(muiNuoc);
	}

	/**
	* Returns the vi nuoc of this thong tin kiem dem nuoc.
	*
	* @return the vi nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getViNuoc() {
		return _thongTinKiemDemNuoc.getViNuoc();
	}

	/**
	* Sets the vi nuoc of this thong tin kiem dem nuoc.
	*
	* @param viNuoc the vi nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public void setViNuoc(java.lang.String viNuoc) {
		_thongTinKiemDemNuoc.setViNuoc(viNuoc);
	}

	/**
	* Returns the loai khac of this thong tin kiem dem nuoc.
	*
	* @return the loai khac of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getLoaiKhac() {
		return _thongTinKiemDemNuoc.getLoaiKhac();
	}

	/**
	* Sets the loai khac of this thong tin kiem dem nuoc.
	*
	* @param loaiKhac the loai khac of this thong tin kiem dem nuoc
	*/
	@Override
	public void setLoaiKhac(java.lang.String loaiKhac) {
		_thongTinKiemDemNuoc.setLoaiKhac(loaiKhac);
	}

	/**
	* Returns the da tung duc khong of this thong tin kiem dem nuoc.
	*
	* @return the da tung duc khong of this thong tin kiem dem nuoc
	*/
	@Override
	public int getDaTungDucKhong() {
		return _thongTinKiemDemNuoc.getDaTungDucKhong();
	}

	/**
	* Sets the da tung duc khong of this thong tin kiem dem nuoc.
	*
	* @param daTungDucKhong the da tung duc khong of this thong tin kiem dem nuoc
	*/
	@Override
	public void setDaTungDucKhong(int daTungDucKhong) {
		_thongTinKiemDemNuoc.setDaTungDucKhong(daTungDucKhong);
	}

	/**
	* Returns the mau nuoc tung co of this thong tin kiem dem nuoc.
	*
	* @return the mau nuoc tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getMauNuocTungCo() {
		return _thongTinKiemDemNuoc.getMauNuocTungCo();
	}

	/**
	* Sets the mau nuoc tung co of this thong tin kiem dem nuoc.
	*
	* @param mauNuocTungCo the mau nuoc tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public void setMauNuocTungCo(java.lang.String mauNuocTungCo) {
		_thongTinKiemDemNuoc.setMauNuocTungCo(mauNuocTungCo);
	}

	/**
	* Returns the mui nuoc tung co of this thong tin kiem dem nuoc.
	*
	* @return the mui nuoc tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getMuiNuocTungCo() {
		return _thongTinKiemDemNuoc.getMuiNuocTungCo();
	}

	/**
	* Sets the mui nuoc tung co of this thong tin kiem dem nuoc.
	*
	* @param muiNuocTungCo the mui nuoc tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public void setMuiNuocTungCo(java.lang.String muiNuocTungCo) {
		_thongTinKiemDemNuoc.setMuiNuocTungCo(muiNuocTungCo);
	}

	/**
	* Returns the vi nuoc tung co of this thong tin kiem dem nuoc.
	*
	* @return the vi nuoc tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getViNuocTungCo() {
		return _thongTinKiemDemNuoc.getViNuocTungCo();
	}

	/**
	* Sets the vi nuoc tung co of this thong tin kiem dem nuoc.
	*
	* @param viNuocTungCo the vi nuoc tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public void setViNuocTungCo(java.lang.String viNuocTungCo) {
		_thongTinKiemDemNuoc.setViNuocTungCo(viNuocTungCo);
	}

	/**
	* Returns the loai khac tung co of this thong tin kiem dem nuoc.
	*
	* @return the loai khac tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getLoaiKhacTungCo() {
		return _thongTinKiemDemNuoc.getLoaiKhacTungCo();
	}

	/**
	* Sets the loai khac tung co of this thong tin kiem dem nuoc.
	*
	* @param loaiKhacTungCo the loai khac tung co of this thong tin kiem dem nuoc
	*/
	@Override
	public void setLoaiKhacTungCo(java.lang.String loaiKhacTungCo) {
		_thongTinKiemDemNuoc.setLoaiKhacTungCo(loaiKhacTungCo);
	}

	/**
	* Returns the co be chua nuoc of this thong tin kiem dem nuoc.
	*
	* @return the co be chua nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public int getCoBeChuaNuoc() {
		return _thongTinKiemDemNuoc.getCoBeChuaNuoc();
	}

	/**
	* Sets the co be chua nuoc of this thong tin kiem dem nuoc.
	*
	* @param coBeChuaNuoc the co be chua nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public void setCoBeChuaNuoc(int coBeChuaNuoc) {
		_thongTinKiemDemNuoc.setCoBeChuaNuoc(coBeChuaNuoc);
	}

	/**
	* Returns the tinh trang be of this thong tin kiem dem nuoc.
	*
	* @return the tinh trang be of this thong tin kiem dem nuoc
	*/
	@Override
	public int getTinhTrangBe() {
		return _thongTinKiemDemNuoc.getTinhTrangBe();
	}

	/**
	* Sets the tinh trang be of this thong tin kiem dem nuoc.
	*
	* @param tinhTrangBe the tinh trang be of this thong tin kiem dem nuoc
	*/
	@Override
	public void setTinhTrangBe(int tinhTrangBe) {
		_thongTinKiemDemNuoc.setTinhTrangBe(tinhTrangBe);
	}

	/**
	* Returns the anh be chua of this thong tin kiem dem nuoc.
	*
	* @return the anh be chua of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getAnhBeChua() {
		return _thongTinKiemDemNuoc.getAnhBeChua();
	}

	/**
	* Sets the anh be chua of this thong tin kiem dem nuoc.
	*
	* @param anhBeChua the anh be chua of this thong tin kiem dem nuoc
	*/
	@Override
	public void setAnhBeChua(java.lang.String anhBeChua) {
		_thongTinKiemDemNuoc.setAnhBeChua(anhBeChua);
	}

	/**
	* Returns the bi mat nuoc of this thong tin kiem dem nuoc.
	*
	* @return the bi mat nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public int getBiMatNuoc() {
		return _thongTinKiemDemNuoc.getBiMatNuoc();
	}

	/**
	* Sets the bi mat nuoc of this thong tin kiem dem nuoc.
	*
	* @param biMatNuoc the bi mat nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public void setBiMatNuoc(int biMatNuoc) {
		_thongTinKiemDemNuoc.setBiMatNuoc(biMatNuoc);
	}

	/**
	* Returns the so lan mat nuoc of this thong tin kiem dem nuoc.
	*
	* @return the so lan mat nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public int getSoLanMatNuoc() {
		return _thongTinKiemDemNuoc.getSoLanMatNuoc();
	}

	/**
	* Sets the so lan mat nuoc of this thong tin kiem dem nuoc.
	*
	* @param soLanMatNuoc the so lan mat nuoc of this thong tin kiem dem nuoc
	*/
	@Override
	public void setSoLanMatNuoc(int soLanMatNuoc) {
		_thongTinKiemDemNuoc.setSoLanMatNuoc(soLanMatNuoc);
	}

	/**
	* Returns the so gio trung binh of this thong tin kiem dem nuoc.
	*
	* @return the so gio trung binh of this thong tin kiem dem nuoc
	*/
	@Override
	public int getSoGioTrungBinh() {
		return _thongTinKiemDemNuoc.getSoGioTrungBinh();
	}

	/**
	* Sets the so gio trung binh of this thong tin kiem dem nuoc.
	*
	* @param soGioTrungBinh the so gio trung binh of this thong tin kiem dem nuoc
	*/
	@Override
	public void setSoGioTrungBinh(int soGioTrungBinh) {
		_thongTinKiemDemNuoc.setSoGioTrungBinh(soGioTrungBinh);
	}

	/**
	* Returns the so se ry dong ho of this thong tin kiem dem nuoc.
	*
	* @return the so se ry dong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getSoSeRyDongHo() {
		return _thongTinKiemDemNuoc.getSoSeRyDongHo();
	}

	/**
	* Sets the so se ry dong ho of this thong tin kiem dem nuoc.
	*
	* @param soSeRyDongHo the so se ry dong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public void setSoSeRyDongHo(java.lang.String soSeRyDongHo) {
		_thongTinKiemDemNuoc.setSoSeRyDongHo(soSeRyDongHo);
	}

	/**
	* Returns the chi so dong ho of this thong tin kiem dem nuoc.
	*
	* @return the chi so dong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getChiSoDongHo() {
		return _thongTinKiemDemNuoc.getChiSoDongHo();
	}

	/**
	* Sets the chi so dong ho of this thong tin kiem dem nuoc.
	*
	* @param chiSoDongHo the chi so dong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public void setChiSoDongHo(java.lang.String chiSoDongHo) {
		_thongTinKiemDemNuoc.setChiSoDongHo(chiSoDongHo);
	}

	/**
	* Returns the anh dong ho of this thong tin kiem dem nuoc.
	*
	* @return the anh dong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getAnhDongHo() {
		return _thongTinKiemDemNuoc.getAnhDongHo();
	}

	/**
	* Sets the anh dong ho of this thong tin kiem dem nuoc.
	*
	* @param anhDongHo the anh dong ho of this thong tin kiem dem nuoc
	*/
	@Override
	public void setAnhDongHo(java.lang.String anhDongHo) {
		_thongTinKiemDemNuoc.setAnhDongHo(anhDongHo);
	}

	/**
	* Returns the ghi chu of this thong tin kiem dem nuoc.
	*
	* @return the ghi chu of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getGhiChu() {
		return _thongTinKiemDemNuoc.getGhiChu();
	}

	/**
	* Sets the ghi chu of this thong tin kiem dem nuoc.
	*
	* @param ghiChu the ghi chu of this thong tin kiem dem nuoc
	*/
	@Override
	public void setGhiChu(java.lang.String ghiChu) {
		_thongTinKiemDemNuoc.setGhiChu(ghiChu);
	}

	/**
	* Returns the da ket thuc dieu tra of this thong tin kiem dem nuoc.
	*
	* @return the da ket thuc dieu tra of this thong tin kiem dem nuoc
	*/
	@Override
	public int getDaKetThucDieuTra() {
		return _thongTinKiemDemNuoc.getDaKetThucDieuTra();
	}

	/**
	* Sets the da ket thuc dieu tra of this thong tin kiem dem nuoc.
	*
	* @param daKetThucDieuTra the da ket thuc dieu tra of this thong tin kiem dem nuoc
	*/
	@Override
	public void setDaKetThucDieuTra(int daKetThucDieuTra) {
		_thongTinKiemDemNuoc.setDaKetThucDieuTra(daKetThucDieuTra);
	}

	/**
	* Returns the ly do khong hoan thanh of this thong tin kiem dem nuoc.
	*
	* @return the ly do khong hoan thanh of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getLyDoKhongHoanThanh() {
		return _thongTinKiemDemNuoc.getLyDoKhongHoanThanh();
	}

	/**
	* Sets the ly do khong hoan thanh of this thong tin kiem dem nuoc.
	*
	* @param lyDoKhongHoanThanh the ly do khong hoan thanh of this thong tin kiem dem nuoc
	*/
	@Override
	public void setLyDoKhongHoanThanh(java.lang.String lyDoKhongHoanThanh) {
		_thongTinKiemDemNuoc.setLyDoKhongHoanThanh(lyDoKhongHoanThanh);
	}

	/**
	* Returns the i mei of this thong tin kiem dem nuoc.
	*
	* @return the i mei of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getIMei() {
		return _thongTinKiemDemNuoc.getIMei();
	}

	/**
	* Sets the i mei of this thong tin kiem dem nuoc.
	*
	* @param iMei the i mei of this thong tin kiem dem nuoc
	*/
	@Override
	public void setIMei(java.lang.String iMei) {
		_thongTinKiemDemNuoc.setIMei(iMei);
	}

	/**
	* Returns the tai khoan of this thong tin kiem dem nuoc.
	*
	* @return the tai khoan of this thong tin kiem dem nuoc
	*/
	@Override
	public java.lang.String getTaiKhoan() {
		return _thongTinKiemDemNuoc.getTaiKhoan();
	}

	/**
	* Sets the tai khoan of this thong tin kiem dem nuoc.
	*
	* @param taiKhoan the tai khoan of this thong tin kiem dem nuoc
	*/
	@Override
	public void setTaiKhoan(java.lang.String taiKhoan) {
		_thongTinKiemDemNuoc.setTaiKhoan(taiKhoan);
	}

	/**
	* Returns the danh gia kiem dem of this thong tin kiem dem nuoc.
	*
	* @return the danh gia kiem dem of this thong tin kiem dem nuoc
	*/
	@Override
	public int getDanhGiaKiemDem() {
		return _thongTinKiemDemNuoc.getDanhGiaKiemDem();
	}

	/**
	* Sets the danh gia kiem dem of this thong tin kiem dem nuoc.
	*
	* @param danhGiaKiemDem the danh gia kiem dem of this thong tin kiem dem nuoc
	*/
	@Override
	public void setDanhGiaKiemDem(int danhGiaKiemDem) {
		_thongTinKiemDemNuoc.setDanhGiaKiemDem(danhGiaKiemDem);
	}

	@Override
	public boolean isNew() {
		return _thongTinKiemDemNuoc.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_thongTinKiemDemNuoc.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _thongTinKiemDemNuoc.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_thongTinKiemDemNuoc.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _thongTinKiemDemNuoc.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _thongTinKiemDemNuoc.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_thongTinKiemDemNuoc.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _thongTinKiemDemNuoc.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_thongTinKiemDemNuoc.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_thongTinKiemDemNuoc.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_thongTinKiemDemNuoc.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ThongTinKiemDemNuocWrapper((ThongTinKiemDemNuoc)_thongTinKiemDemNuoc.clone());
	}

	@Override
	public int compareTo(
		dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc thongTinKiemDemNuoc) {
		return _thongTinKiemDemNuoc.compareTo(thongTinKiemDemNuoc);
	}

	@Override
	public int hashCode() {
		return _thongTinKiemDemNuoc.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> toCacheModel() {
		return _thongTinKiemDemNuoc.toCacheModel();
	}

	@Override
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc toEscapedModel() {
		return new ThongTinKiemDemNuocWrapper(_thongTinKiemDemNuoc.toEscapedModel());
	}

	@Override
	public dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc toUnescapedModel() {
		return new ThongTinKiemDemNuocWrapper(_thongTinKiemDemNuoc.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _thongTinKiemDemNuoc.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _thongTinKiemDemNuoc.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_thongTinKiemDemNuoc.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ThongTinKiemDemNuocWrapper)) {
			return false;
		}

		ThongTinKiemDemNuocWrapper thongTinKiemDemNuocWrapper = (ThongTinKiemDemNuocWrapper)obj;

		if (Validator.equals(_thongTinKiemDemNuoc,
					thongTinKiemDemNuocWrapper._thongTinKiemDemNuoc)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ThongTinKiemDemNuoc getWrappedThongTinKiemDemNuoc() {
		return _thongTinKiemDemNuoc;
	}

	@Override
	public ThongTinKiemDemNuoc getWrappedModel() {
		return _thongTinKiemDemNuoc;
	}

	@Override
	public void resetOriginalValues() {
		_thongTinKiemDemNuoc.resetOriginalValues();
	}

	private ThongTinKiemDemNuoc _thongTinKiemDemNuoc;
}