package dtt.nuocsach.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connect {
	private static final String URLAGGREGATE = "http://hanoi.dtt.vn:9182/ODKAggregate/odktables/tables/tables/";
	private static final String DATABASE_URL = "jdbc:mysql://192.168.1.188/ns_odk";
	private static final String USERNAME = "ns_dli";
	private static final String PASSWORD = "ns123";

	public static Connection getconect() throws SQLException {
		Properties info = new Properties();
		info.put("user", USERNAME);
		info.put("password", PASSWORD);
		System.out.println("Connecting database...");
		java.sql.Connection conn = DriverManager.getConnection(DATABASE_URL,
				info);
		return conn;

	}

	public static String getURL() {
		return URLAGGREGATE;
	}
}
